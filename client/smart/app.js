import React, { Component } from 'react'
import { connect } from 'react-redux'

import { partial } from 'ramda'
import { noop } from 'lodash'

import Entry from 'dumb/entry'
import NameSelector from 'dumb/name-selector'
import ShareScreen from 'dumb/share-screen'
import SuccessScreen from 'dumb/success-screen'
import Congratulation from 'dumb/congratulation'

import { sendEmail, setRoute, toggleNameSelector, setName } from 'actions/app'

class App extends Component {
  render() {
    const { recipientName, setRoute, route, setName, sendEmail
          , nameSelectorIsOpened, toggleNameSelector} = this.props

    return (
      <div id="wrapper">{
        { entry:
          <Entry
            onClickCreate={
              recipientName
                ? partial(setRoute, ['sharer'])
                : toggleNameSelector
            }
            onClickSelectName={toggleNameSelector}
            recipientName={recipientName}
            />

        , sharer:
          <ShareScreen
            recipientName={recipientName}
            onShareCompletion={noop}
            goback={() => { setRoute('entry'); toggleNameSelector()} }
            sendEmail={sendEmail}
            />

        , congratulation:
          <Congratulation
            onClickCreate={() => (setName(null), setRoute('entry'))}
            recipientName={recipientName}
            />
        }[route]      
      }
      <NameSelector
        onNameSelect={setName}
        isOpened={nameSelectorIsOpened}
        toggleNameSelector={toggleNameSelector}
        />
      </div>
    )
  }
}

export default connect
  ( (state, ownProps) => (
      { route: state.route
      , recipientName: state.recipientName
      , nameSelectorIsOpened: state.nameSelectorIsOpened
      }
    )
  , (dispatch, ownProps) => (
      { sendEmail: data => dispatch(sendEmail(data))
      , setRoute: route => dispatch(setRoute(route))
      , toggleNameSelector: () => dispatch(toggleNameSelector())
      , setName: name => dispatch(setName(name))
      }
    )
  )(App)
