import $ from 'jquery'

export const toggleNameSelector = () => {
  return {
    type: 'TOGGLE_NAME_SELECTOR',
  }
}

export const setRoute = route => {
  return {
    type: 'SET_ROUTE',
    route
  }
}

export const toggleLoading = () => {
  return {
    type: 'TOGGLE_LOADING',
  }
}

export const sendEmail = (data) => {
  return dispatch => {
    dispatch(toggleLoading())

    const handle = () => {
      dispatch(toggleLoading())
      dispatch(setRoute('sent'))
      dispatch(setName(null))
    }
    
    $.ajax({
      type: 'POST',
      url: 'https://contentscale.ru/api/store/9/',
      data,
      dataType: 'json',
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      success: handle
    })
  }
}

export const setName = name => {
  return {
    type: 'SET_NAME',
    name
  }
}
