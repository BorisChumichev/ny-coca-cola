import 'index.sass'

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import app from 'reducers/app'
import App from 'smart/app'
import createLogger from 'redux-logger'
import ReduxThunk from 'redux-thunk'

import { isMobile } from './util.js'

const logger = createLogger()
let store = createStore(
  app,
  applyMiddleware(ReduxThunk, logger)
)

const rootElement = document.getElementById('app')

render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
)

if (isMobile()) rootElement.className = 'mobile'
