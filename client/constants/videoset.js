export default [
  {
    "name": "Айгуль",
    "video": "Deer_Айгуль_185"
  },
  {
    "name": "Алina",
    "video": "Deer_Алiна_126"
  },
  {
    "name": "Алина",
    "video": "Deer_Алина_126"
  },
  {
    "name": "Алина (Алинка)",
    "video": "Deer_Алинка_126"
  },
  {
    "name": "Алина (Алиночка)",
    "video": "Deer_Алиночка_126"
  },
  {
    "name": "Алиса (Alice)",
    "video": "Deer_Alice_156"
  },
  {
    "name": "Алиса (Alisa)",
    "video": "Deer_Alisa_156"
  },
  {
    "name": "Алиса",
    "video": "Deer_Алиса_156"
  },
  {
    "name": "Алина (Alina)",
    "video": "Deer_Alina_126"
  },
  {
    "name": "Алия",
    "video": "Deer_Алия_189"
  },
  {
    "name": "Алла",
    "video": "Deer_Алла_148"
  },
  {
    "name": "Алла (Alla)",
    "video": "Deer_Alla_148"
  },
  {
    "name": "Альбина",
    "video": "Deer_Альбина_161"
  },
  {
    "name": "Ангелина",
    "video": "Deer_Ангелина_166"
  },
  {
    "name": "Ангелина (Angelina)",
    "video": "Deer_Angelina_166"
  },
  {
    "name": "Анжела",
    "video": "Deer_Анжела_174"
  },
  {
    "name": "Анжелика",
    "video": "Deer_Анжелика_165"
  },
  {
    "name": "Арина",
    "video": "Deer_Арина_170"
  },
  {
    "name": "Ася",
    "video": "Deer_Ася_194"
  },
  {
    "name": "Вера",
    "video": "Deer_Вера_144"
  },
  {
    "name": "Вера (Vera)",
    "video": "Deer_Vera_144"
  },
  {
    "name": "Вероника",
    "video": "Deer_Вероника_146"
  },
  {
    "name": "Вероника (Veronika)",
    "video": "Deer_Veronika_146"
  },
  {
    "name": "Вероника (Вероничка)",
    "video": "Deer_Вероничка_146"
  },
  {
    "name": "Галя",
    "video": "Deer_Галя_191"
  },
  {
    "name": "Гузель",
    "video": "Deer_Гузель_193"
  },
  {
    "name": "Гульнара",
    "video": "Deer_Гульнара_196"
  },
  {
    "name": "Диана",
    "video": "Deer_Диана_147"
  },
  {
    "name": "Диана (Diana)",
    "video": "Deer_Diana_147"
  },
  {
    "name": "Дина",
    "video": "Deer_Дина_177"
  },
  {
    "name": "Дина (Dina)",
    "video": "Deer_Dina_177"
  },
  {
    "name": "Динара",
    "video": "Deer_Динара_197"
  },
  {
    "name": "Дорогая",
    "video": "Deer_Dear_200"
  },
  {
    "name": "Жанна",
    "video": "Deer_Жанна_158"
  },
  {
    "name": "Зоя",
    "video": "Deer_Зоя_192"
  },
  {
    "name": "Илона",
    "video": "Deer_Илона_195"
  },
  {
    "name": "Инга",
    "video": "Deer_Инга_188"
  },
  {
    "name": "Карина",
    "video": "Deer_Карина_151"
  },
  {
    "name": "Карина (Каринка)",
    "video": "Deer_Каринка_151"
  },
  {
    "name": "Карина (Кариночка)",
    "video": "Deer_Кариночка_151"
  },
  {
    "name": "Карина (Karina)",
    "video": "Deer_Karina_151"
  },
  {
    "name": "Кира",
    "video": "Deer_Кира_171"
  },
  {
    "name": "Лариса",
    "video": "Deer_Лариса_141"
  },
  {
    "name": "Лариса (Larisa)",
    "video": "Deer_Larisa_141"
  },
  {
    "name": "Лилия",
    "video": "Deer_Лилия_150"
  },
  {
    "name": "Любовь",
    "video": "Deer_Любовь_142"
  },
  {
    "name": "Майя",
    "video": "Deer_Майа_199"
  },
  {
    "name": "Нина",
    "video": "Deer_Нина_152"
  },
  {
    "name": "Нина (Nina)",
    "video": "Deer_Nina_152"
  },
  {
    "name": "Регина",
    "video": "Deer_Регина_169"
  },
  {
    "name": "Регина (Regina)",
    "video": "Deer_Regina_169"
  },
  {
    "name": "Тамара",
    "video": "Deer_Тамара_167"
  },
  {
    "name": "Ульяна",
    "video": "Deer_Ульяна_173"
  },
  {
    "name": "Элина",
    "video": "Deer_Элина_187"
  },
  {
    "name": "Эльвира",
    "video": "Deer_Эльвира_162"
  },
  {
    "name": "Александра",
    "video": "Santa_Александра_115"
  },
  {
    "name": "Александра (Aleksandra)",
    "video": "Santa_Aleksandra_115"
  },
  {
    "name": "Александра (Alexandra)",
    "video": "Santa_Alexandra_115"
  },
  {
    "name": "Алеся",
    "video": "Santa_Алеся_135"
  },
  {
    "name": "Анастасия",
    "video": "Santa_Анастасия_109"
  },
  {
    "name": "Анастасия (Анастасiя)",
    "video": "Santa_Анастасiя_109"
  },
  {
    "name": "Анастасия (Anastasia)",
    "video": "Santa_Anastasia_109"
  },
  {
    "name": "Анастасия (Anastasiya)",
    "video": "Santa_Anastasiya_109"
  },
  {
    "name": "Анна",
    "video": "Santa_Анна_107"
  },
  {
    "name": "Анна (Anna)",
    "video": "Santa_Anna_107"
  },
  {
    "name": "Антонина",
    "video": "Santa_Антонина_176"
  },
  {
    "name": "Валентина",
    "video": "Santa_Валентина_138"
  },
  {
    "name": "Валентина (Valentina)",
    "video": "Santa_Valentina_138"
  },
  {
    "name": "Валерия",
    "video": "Santa_Валерия_140"
  },
  {
    "name": "Валерия (Valeria)",
    "video": "Santa_Valeria_140"
  },
  {
    "name": "Виктория",
    "video": "Santa_Виктория_113"
  },
  {
    "name": "Виктория (Viktoriya)",
    "video": "Santa_Victoriya_113"
  },
  {
    "name": "Виктория (Viktoria)",
    "video": "Santa_Viktoria_113"
  },
  {
    "name": "Виктория (Victoria)",
    "video": "Santa_Victoria_113"
  },
  {
    "name": "Виктория (Вiкторiя)",
    "video": "Santa_Вiкторiя_113"
  },
  {
    "name": "Галина",
    "video": "Santa_Галина_132"
  },
  {
    "name": "Галина (Galina)",
    "video": "Santa_Galina_132"
  },
  {
    "name": "Галина (Галинка)",
    "video": "Santa_Галинка_132"
  },
  {
    "name": "Дарья",
    "video": "Santa_Дарья_123"
  },
  {
    "name": "Дарья (Daria)",
    "video": "Santa_Daria_123"
  },
  {
    "name": "Евгения",
    "video": "Santa_Евгения_116"
  },
  {
    "name": "Евгения (Evgenia)",
    "video": "Santa_Evgenia_116"
  },
  {
    "name": "Евгения (Evgeniya)",
    "video": "Santa_Evgeniya_116"
  },
  {
    "name": "Екатерина",
    "video": "Santa_Екатерина_108"
  },
  {
    "name": "Екатерина (Ekaterina)",
    "video": "Santa_Ekaterina_108"
  },
  {
    "name": "Елена",
    "video": "Santa_Елена_101"
  },
  {
    "name": "Еленa (Elena)",
    "video": "Santa_Elena_101"
  },
  {
    "name": "Елизавета",
    "video": "Santa_Елизавета_157"
  },
  {
    "name": "Ирина",
    "video": "Santa_Ирина_103"
  },
  {
    "name": "Ирина (Irina)",
    "video": "Santa_Iрина_103"
  },
  {
    "name": "Ирина (Ірина)",
    "video": "Santa_Iрина_103"
  },
  {
    "name": "Инна",
    "video": "Santa_Инна_134"
  },
  {
    "name": "Инна (Inna)",
    "video": "Santa_Inna_134"
  },
  {
    "name": "Инна (Инночка)",
    "video": "Santa_Инночка_134"
  },
  {
    "name": "Инна (Інна)",
    "video": "Santa_Iнна_134"
  },
  {
    "name": "Катерина",
    "video": "Santa_Катерина_136"
  },
  {
    "name": "Катерина (Katerina)",
    "video": "Santa_Катерина_136"
  },
  {
    "name": "Катерина (Катеринка)",
    "video": "Santa_Катеринка_136"
  },
  {
    "name": "Кристина",
    "video": "Santa_Кристина_119"
  },
  {
    "name": "Кристина (Kristina)",
    "video": "Santa_Kristina_119"
  },
  {
    "name": "Кристина (Christina)",
    "video": "Santa_Christina_119"
  },
  {
    "name": "Кристина (Кристинка)",
    "video": "Santa_Кристинка_119"
  },
  {
    "name": "Кристина (Кристиночка)",
    "video": "Santa_Кристиночка_119"
  },
  {
    "name": "Ксения",
    "video": "Santa_Ксения_118"
  },
  {
    "name": "Ксения (Kseniya)",
    "video": "Santa_Kseniya_118"
  },
  {
    "name": "Ксения (Ksenia)",
    "video": "Santa_Ksenia_118"
  },
  {
    "name": "Лидия",
    "video": "Santa_Лидия_168"
  },
  {
    "name": "Людмила",
    "video": "Santa_Людмила_122"
  },
  {
    "name": "Людмила (Ludmila)",
    "video": "Santa_Ludmila_122"
  },
  {
    "name": "Мария",
    "video": "Santa_Мария_112"
  },
  {
    "name": "Мария (Maria)",
    "video": "Santa_Maria_112"
  },
  {
    "name": "Мария (Марiя)",
    "video": "Santa_Марiя_112"
  },
  {
    "name": "Маргарита",
    "video": "Santa_Маргарита_149"
  },
  {
    "name": "Маргарита (Margarita)",
    "video": "Santa_Margarita_149"
  },
  {
    "name": "Надежда",
    "video": "Santa_Надежда_124"
  },
  {
    "name": "Наталья",
    "video": "Santa_Наталья_106"
  },
  {
    "name": "Наталья (Natalia)",
    "video": "Santa_Natalia_129"
  },
  {
    "name": "Наталья (Наталия)",
    "video": "Santa_Наталия_106"
  },
  {
    "name": "Наталья (Natalya)",
    "video": "Santa_Natalya_106"
  },
  {
    "name": "Наталья (Nataliya)",
    "video": "Santa_Nataliya_129"
  },
  {
    "name": "Наталья (Наталiя)",
    "video": "Santa_Наталiя_106"
  },
  {
    "name": "Оксана",
    "video": "Santa_Оксана_114"
  },
  {
    "name": "Оксана (Oksana)",
    "video": "Santa_Oksana_114"
  },
  {
    "name": "Оксана (Оксанка)",
    "video": "Santa_Оксанка_114"
  },
  {
    "name": "Оксана (Оксаночка)",
    "video": "Santa_Оксаночка_114"
  },
  {
    "name": "Олеся",
    "video": "Santa_Олеся_135"
  },
  {
    "name": "Олеся (Olesya)",
    "video": "Santa_Olesya_135"
  },
  {
    "name": "Ольга",
    "video": "Santa_Ольга_102"
  },
  {
    "name": "Ольга (Olga)",
    "video": "Santa_Olga_102"
  },
  {
    "name": "Светлана",
    "video": "Santa_Светлана_110"
  },
  {
    "name": "Светлана (Svetlana)",
    "video": "Santa_Svetlana_110"
  },
  {
    "name": "Светлана (Світлана)",
    "video": "Santa_Свiтлана_110"
  },
  {
    "name": "Светлана (Светланка)",
    "video": "Santa_Светланка_110"
  },
  {
    "name": "София",
    "video": "Santa_София_181"
  },
  {
    "name": "Софья",
    "video": "Santa_Софья_183"
  },
  {
    "name": "Татьяна",
    "video": "Santa_Татьяна_137"
  },
  {
    "name": "Татьяна (Tatiana)",
    "video": "Santa_Tatiana_104"
  },
  {
    "name": "Татьяна (Татьянка)",
    "video": "Santa_Татьянка_104"
  },
  {
    "name": "Татьяна (Tatyana)",
    "video": "Santa_Tatyana_104"
  },
  {
    "name": "Татьяна (Tatjana)",
    "video": "Santa_Tatjana_104"
  },
  {
    "name": "Юлия",
    "video": "Santa_Юлия_105"
  },
  {
    "name": "Юлия (Julia)",
    "video": "Santa_Julia_125"
  },
  {
    "name": "Юлия (Юлія)",
    "video": "Santa_Юлiя_125"
  },
  {
    "name": "Юлия (Yulia)",
    "video": "Santa_Yulia_125"
  },
  {
    "name": "Юлия (Yuliya)",
    "video": "Santa_Yuliya_125"
  },
  {
    "name": "Алекс",
    "video": "Snowman_Алекс_71"
  },
  {
    "name": "Aleks",
    "video": "Snowman_Aleks_71"
  },
  {
    "name": "Alex",
    "video": "Snowman_Alex_71"
  },
  {
    "name": "Александер",
    "video": "Snowman_Александер_1"
  },
  {
    "name": "Alexander",
    "video": "Snowman_Alexander_1"
  },
  {
    "name": "Александр",
    "video": "Snowman_Aleksandr_1"
  },
  {
    "name": "Alexandr",
    "video": "Snowman_Alexandr_1"
  },
  {
    "name": "Александр",
    "video": "Snowman_Александр_1"
  },
  {
    "name": "Алексей",
    "video": "Snowman_Алексей_4"
  },
  {
    "name": "Алексей (Aleksei)",
    "video": "Snowman_Aleksei_4"
  },
  {
    "name": "Алексей (Aleksey)",
    "video": "Snowman_Aleksey_4"
  },
  {
    "name": "Алексей (Aleksei)",
    "video": "Snowman_Alexei_4"
  },
  {
    "name": "Алексей (Alexey)",
    "video": "Snowman_Alexey_4"
  },
  {
    "name": "Альберт",
    "video": "Snowman_Альберт_77"
  },
  {
    "name": "Анатолий",
    "video": "Snowman_Анатолий_36"
  },
  {
    "name": "Анатолий (Анатолiй)",
    "video": "Snowman_Анатолiй_36"
  },
  {
    "name": "Андрей",
    "video": "Snowman_Андрей_3"
  },
  {
    "name": "Андрей (Andrei)",
    "video": "Snowman_Andrei_3"
  },
  {
    "name": "Андрей (Andrej)",
    "video": "Snowman_Andrej_3"
  },
  {
    "name": "Андрей (Andrey)",
    "video": "Snowman_Andrey_3"
  },
  {
    "name": "Андрей (Андрюха)",
    "video": "Snowman_Андрюха_3"
  },
  {
    "name": "Андрей (Андрюша)",
    "video": "Snowman_Андрюша_3"
  },
  {
    "name": "Андрей (Андрюшка)",
    "video": "Snowman_Андрюшка_3"
  },
  {
    "name": "Антон",
    "video": "Snowman_Антон_12"
  },
  {
    "name": "Антон (Anton)",
    "video": "Snowman_Anton_12"
  },
  {
    "name": "Антон (Aнтоха)",
    "video": "Snowman_Антоха_12"
  },
  {
    "name": "Антон (Aнтоша)",
    "video": "Snowman_Антоша_12"
  },
  {
    "name": "Антон (Aнтошка)",
    "video": "Snowman_Антошка_12"
  },
  {
    "name": "Аркадий",
    "video": "Snowman_Аркадий_93"
  },
  {
    "name": "Арсений",
    "video": "Snowman_Арсений_98"
  },
  {
    "name": "Artem",
    "video": "Snowman_Artem_27"
  },
  {
    "name": "Артем",
    "video": "Snowman_Артем_27"
  },
  {
    "name": "Артём",
    "video": "Snowman_Артём_27"
  },
  {
    "name": "Артёмка",
    "video": "Snowman_Артёмка_27"
  },
  {
    "name": "Артур",
    "video": "Snowman_Артур_34"
  },
  {
    "name": "Артур (Arthur)",
    "video": "Snowman_Arthur_34"
  },
  {
    "name": "Артур (Artur)",
    "video": "Snowman_Artur_34"
  },
  {
    "name": "Богдан",
    "video": "Snowman_Богдан_64"
  },
  {
    "name": "Богдан (Bogdan)",
    "video": "Snowman_Bogdan_64"
  },
  {
    "name": "Борис",
    "video": "Snowman_Борис_56"
  },
  {
    "name": "Борис (Boris)",
    "video": "Snowman_Boris_56"
  },
  {
    "name": "Борис (Боря)",
    "video": "Snowman_Боря_56"
  },
  {
    "name": "Вадим",
    "video": "Snowman_Вадим_29"
  },
  {
    "name": "Вадим (Вадик)",
    "video": "Snowman_Вадик_29"
  },
  {
    "name": "Вадим (Вадiм)",
    "video": "Snowman_Вадiм_29"
  },
  {
    "name": "Вадим (Vadim)",
    "video": "Snowman_Vadim_29"
  },
  {
    "name": "Валентин",
    "video": "Snowman_Валентин_63"
  },
  {
    "name": "Валера",
    "video": "Snowman_Валера_62"
  },
  {
    "name": "Валера (Valera)",
    "video": "Snowman_Valera_62"
  },
  {
    "name": "Валерий",
    "video": "Snowman_Валерий_44"
  },
  {
    "name": "Ваня",
    "video": "Snowman_Ваня_49"
  },
  {
    "name": "Ваня (Ванек)",
    "video": "Snowman_Ванек_49"
  },
  {
    "name": "Ваня (Ванёк)",
    "video": "Snowman_Ванёк_49"
  },
  {
    "name": "Ваня (Ванька)",
    "video": "Snowman_Ванька_49"
  },
  {
    "name": "Василий",
    "video": "Snowman_Василий_31"
  },
  {
    "name": "Вася",
    "video": "Snowman_Вася_37"
  },
  {
    "name": "Вася (Vasya)",
    "video": "Snowman_Vasya_37"
  },
  {
    "name": "Вася (Васёк)",
    "video": "Snowman_Васёк_37"
  },
  {
    "name": "Вася (Васька)",
    "video": "Snowman_Васька_37"
  },
  {
    "name": "Виктор",
    "video": "Snowman_Виктор_22"
  },
  {
    "name": "Виктор (Вiктор)",
    "video": "Snowman_Вiктор_22"
  },
  {
    "name": "Виктор (Victor)",
    "video": "Snowman_Victor_22"
  },
  {
    "name": "Виктор (Viktor)",
    "video": "Snowman_Viktor_22"
  },
  {
    "name": "Витя",
    "video": "Snowman_Витя_78"
  },
  {
    "name": "Витя (Витек)",
    "video": "Snowman_Витек_78"
  },
  {
    "name": "Витя (Витёк)",
    "video": "Snowman_Витёк_78"
  },
  {
    "name": "Виталий",
    "video": "Snowman_Виталий_23"
  },
  {
    "name": "Виталий (Vitaliy)",
    "video": "Snowman_Vitaliy_23"
  },
  {
    "name": "Виталий (Вiталiй)",
    "video": "Snowman_Вiталiй_23"
  },
  {
    "name": "Виталик",
    "video": "Snowman_Виталик_66"
  },
  {
    "name": "Виталик (Vitalik)",
    "video": "Snowman_Vitalik_66"
  },
  {
    "name": "Виталик (Вiталiк)",
    "video": "Snowman_Вiталiк_66"
  },
  {
    "name": "Влад",
    "video": "Snowman_Влад_33"
  },
  {
    "name": "Влад (Vlad)",
    "video": "Snowman_Vlad_33"
  },
  {
    "name": "Влад (Владик)",
    "video": "Snowman_Владик_33"
  },
  {
    "name": "Владислав",
    "video": "Snowman_Владислав_40"
  },
  {
    "name": "Владислав (Vladislav)",
    "video": "Snowman_Vladislav_40"
  },
  {
    "name": "Владимир",
    "video": "Snowman_Владимир_8"
  },
  {
    "name": "Владимир (Vladimir)",
    "video": "Snowman_Vladimir_8"
  },
  {
    "name": "Вова",
    "video": "Snowman_Вова_48"
  },
  {
    "name": "Вова (Vova)",
    "video": "Snowman_Vova_48"
  },
  {
    "name": "Вова (Вован)",
    "video": "Snowman_Вован_48"
  },
  {
    "name": "Вова (Вовка)",
    "video": "Snowman_Вовка_48"
  },
  {
    "name": "Вова (Вовчик)",
    "video": "Snowman_Вовчик_48"
  },
  {
    "name": "Володя",
    "video": "Snowman_Володя_91"
  },
  {
    "name": "Вячеслав",
    "video": "Snowman_Вячеслав_35"
  },
  {
    "name": "Гена",
    "video": "Snowman_Гена_96"
  },
  {
    "name": "Генадий",
    "video": "Snowman_Генадий_70"
  },
  {
    "name": "Геннадий",
    "video": "Snowman_Геннадий_70"
  },
  {
    "name": "Георгий",
    "video": "Snowman_Георгий_60"
  },
  {
    "name": "Глеб",
    "video": "Snowman_Глеб_76"
  },
  {
    "name": "Григорий",
    "video": "Snowman_Григорий_61"
  },
  {
    "name": "Давид",
    "video": "Snowman_Давид_88"
  },
  {
    "name": "David",
    "video": "Snowman_David_88"
  },
  {
    "name": "Дамир",
    "video": "Snowman_Дамир_95"
  },
  {
    "name": "Даниил",
    "video": "Snowman_Даниил_68"
  },
  {
    "name": "Данил",
    "video": "Snowman_Данил_65"
  },
  {
    "name": "Данил (Danil)",
    "video": "Snowman_Danil_65"
  },
  {
    "name": "Данила",
    "video": "Snowman_Данила_87"
  },
  {
    "name": "Денис",
    "video": "Snowman_Денис_10"
  },
  {
    "name": "Денис (Denis)",
    "video": "Snowman_Denis_10"
  },
  {
    "name": "Денис (Дениска)",
    "video": "Snowman_Дениска_10"
  },
  {
    "name": "Дима",
    "video": "Snowman_Дима_18"
  },
  {
    "name": "Дима (Dima)",
    "video": "Snowman_Dima_18"
  },
  {
    "name": "Дима (Дiма)",
    "video": "Snowman_Дiма_18"
  },
  {
    "name": "Дима (Диман)",
    "video": "Snowman_Диман_18"
  },
  {
    "name": "Дима (Димка)",
    "video": "Snowman_Димка_18"
  },
  {
    "name": "Дима (Dimka)",
    "video": "Snowman_Dimka_18"
  },
  {
    "name": "Дима (Димон)",
    "video": "Snowman_Димон_18"
  },
  {
    "name": "Дима (Димон)",
    "video": "Snowman_Dimon_18"
  },
  {
    "name": "Дмитрий",
    "video": "Snowman_Дмитрий_5"
  },
  {
    "name": "Дмитрий (Dmitriy)",
    "video": "Snowman_Dmitriy_5"
  },
  {
    "name": "Дмитрий (Dmitry)",
    "video": "Snowman_Dmitry_5"
  },
  {
    "name": "Дружище",
    "video": "Snowman_Друг_100"
  },
  {
    "name": "Евгений",
    "video": "Snowman_Евгений_6"
  },
  {
    "name": "Евгений (Evgeniy)",
    "video": "Snowman_Evgeniy_6"
  },
  {
    "name": "Евгений (Evgeny)",
    "video": "Snowman_Evgeny_6"
  },
  {
    "name": "Егор",
    "video": "Snowman_Егор_41"
  },
  {
    "name": "Egor",
    "video": "Snowman_Egor_41"
  },
  {
    "name": "Игорь",
    "video": "Snowman_Игорь_11"
  },
  {
    "name": "Игорь (Igor)",
    "video": "Snowman_Igor_11"
  },
  {
    "name": "Игорь (Iгоp)",
    "video": "Snowman_Iгор_11"
  },
  {
    "name": "Игорь (Игорек)",
    "video": "Snowman_Игорек_11"
  },
  {
    "name": "Игорь (Игорёк)",
    "video": "Snowman_Игорёк_11"
  },
  {
    "name": "Иван",
    "video": "Snowman_Иван_9"
  },
  {
    "name": "Иван (Ivan)",
    "video": "Snowman_Ivan_9"
  },
  {
    "name": "Иван (Iван)",
    "video": "Snowman_Iван_9"
  },
  {
    "name": "Женя",
    "video": "Snowman_Женя_30"
  },
  {
    "name": "Женя (Жека)",
    "video": "Snowman_Жека_30"
  },
  {
    "name": "Женя (Женек)",
    "video": "Snowman_Женек_30"
  },
  {
    "name": "Женя (Женёк)",
    "video": "Snowman_Женёк_30"
  },
  {
    "name": "Женя (Женька)",
    "video": "Snowman_Женька_30"
  },
  {
    "name": "Ильдар",
    "video": "Snowman_Ильдар_84"
  },
  {
    "name": "Илья",
    "video": "Snowman_Илья_19"
  },
  {
    "name": "Илья (Ilya)",
    "video": "Snowman_Ilya_19"
  },
  {
    "name": "Илья (Илюха)",
    "video": "Snowman_Илюха_19"
  },
  {
    "name": "Кирил",
    "video": "Snowman_Кирил_25"
  },
  {
    "name": "Кирилл",
    "video": "Snowman_Кирилл_25"
  },
  {
    "name": "Кирилл (Kirill)",
    "video": "Snowman_Kirill_25"
  },
  {
    "name": "Коля",
    "video": "Snowman_Коля_52"
  },
  {
    "name": "Коля (Колян)",
    "video": "Snowman_Колян_52"
  },
  {
    "name": "Константин",
    "video": "Snowman_Константин_28"
  },
  {
    "name": "Константин (Konstantin)",
    "video": "Snowman_Konstantin_28"
  },
  {
    "name": "Костя",
    "video": "Snowman_Костя_50"
  },
  {
    "name": "Костя (Костик)",
    "video": "Snowman_Костик_50"
  },
  {
    "name": "Костя (Костян)",
    "video": "Snowman_Костян_50"
  },
  {
    "name": "Костя (Kostya)",
    "video": "Snowman_Kostya_50"
  },
  {
    "name": "Лев",
    "video": "Snowman_Лев_99"
  },
  {
    "name": "Леонид",
    "video": "Snowman_Леонид_59"
  },
  {
    "name": "Леонид (Leonid)",
    "video": "Snowman_Leonid_59"
  },
  {
    "name": "Леша",
    "video": "Snowman_Леша_85"
  },
  {
    "name": "Леша (Лёша)",
    "video": "Snowman_Лёша_85"
  },
  {
    "name": "Леша (Алеша)",
    "video": "Snowman_Алеша_85"
  },
  {
    "name": "Леша (Алёша)",
    "video": "Snowman_Алёша_85"
  },
  {
    "name": "Леша (Лешка)",
    "video": "Snowman_Лешка_85"
  },
  {
    "name": "Леша (Лёшка)",
    "video": "Snowman_Лёшка_85"
  },
  {
    "name": "Леха",
    "video": "Snowman_Леха_69"
  },
  {
    "name": "Лёха",
    "video": "Snowman_Лёха_69"
  },
  {
    "name": "Макс",
    "video": "Snowman_Макс_32"
  },
  {
    "name": "Макс (Maks)",
    "video": "Snowman_Maks_32"
  },
  {
    "name": "Макс (Max)",
    "video": "Snowman_Max_32"
  },
  {
    "name": "Максим",
    "video": "Snowman_Максим_7"
  },
  {
    "name": "Максим (Maksim)",
    "video": "Snowman_Maksim_7"
  },
  {
    "name": "Максим (Maxim)",
    "video": "Snowman_Maxim_7"
  },
  {
    "name": "Максим (Максимка)",
    "video": "Snowman_Максимка_7"
  },
  {
    "name": "Марат",
    "video": "Snowman_Марат_67"
  },
  {
    "name": "Марат (Marat)",
    "video": "Snowman_Marat_67"
  },
  {
    "name": "Марк",
    "video": "Snowman_Марк_81"
  },
  {
    "name": "Mark",
    "video": "Snowman_Mark_81"
  },
  {
    "name": "Михаил",
    "video": "Snowman_Михаил_14"
  },
  {
    "name": "Михаил (Mahail)",
    "video": "Snowman_Mihail_14"
  },
  {
    "name": "Михаил (Makhail)",
    "video": "Snowman_Mikhail_14"
  },
  {
    "name": "Миша",
    "video": "Snowman_Миша_47"
  },
  {
    "name": "Миша (Misha)",
    "video": "Snowman_Misha_47"
  },
  {
    "name": "Миша (Miша)",
    "video": "Snowman_Мiша_47"
  },
  {
    "name": "Миша (Мишаня)",
    "video": "Snowman_Мишаня_47"
  },
  {
    "name": "Миша (Мишка)",
    "video": "Snowman_Мишка_47"
  },
  {
    "name": "Миша (Миха)",
    "video": "Snowman_Миха_47"
  },
  {
    "name": "Назар",
    "video": "Snowman_Назар_97"
  },
  {
    "name": "Никита",
    "video": "Snowman_Никита_24"
  },
  {
    "name": "Nikita",
    "video": "Snowman_Nikita_24"
  },
  {
    "name": "Никитка",
    "video": "Snowman_Никитка_24"
  },
  {
    "name": "Никитос",
    "video": "Snowman_Никитос_24"
  },
  {
    "name": "Николай",
    "video": "Snowman_Николай_17"
  },
  {
    "name": "Николай (Nikolay)",
    "video": "Snowman_Nikolay_17"
  },
  {
    "name": "Олег",
    "video": "Snowman_Олег_13"
  },
  {
    "name": "Олег (Oleg)",
    "video": "Snowman_Oleg_13"
  },
  {
    "name": "Олег (Олежка)",
    "video": "Snowman_Олежка_13"
  },
  {
    "name": "Олександр",
    "video": "Snowman_Олександр_1"
  },
  {
    "name": "Павел",
    "video": "Snowman_Павел_16"
  },
  {
    "name": "Pavel",
    "video": "Snowman_Pavel_16"
  },
  {
    "name": "Паша",
    "video": "Snowman_Паша_46"
  },
  {
    "name": "Паша (Pasha)",
    "video": "Snowman_Pasha_46"
  },
  {
    "name": "Паша (Пашка)",
    "video": "Snowman_Пашка_46"
  },
  {
    "name": "Паша (Пашок)",
    "video": "Snowman_Пашок_46"
  },
  {
    "name": "Петр (Petr)",
    "video": "Snowman_Petr_55"
  },
  {
    "name": "Петр",
    "video": "Snowman_Петр_55"
  },
  {
    "name": "Пётр",
    "video": "Snowman_Пётр_55"
  },
  {
    "name": "Петя",
    "video": "Snowman_Петя_75"
  },
  {
    "name": "Рамиль",
    "video": "Snowman_Рамиль_90"
  },
  {
    "name": "Ренат",
    "video": "Snowman_Ренат_79"
  },
  {
    "name": "Ринат",
    "video": "Snowman_Ринат_79"
  },
  {
    "name": "Рома",
    "video": "Snowman_Рома_43"
  },
  {
    "name": "Рома",
    "video": "Snowman_Roma_43"
  },
  {
    "name": "Рома (Ромка)",
    "video": "Snowman_Ромка_43"
  },
  {
    "name": "Роман",
    "video": "Snowman_Роман_15"
  },
  {
    "name": "Роман (Roman)",
    "video": "Snowman_Roman_43"
  },
  {
    "name": "Руслан",
    "video": "Snowman_Руслан_26"
  },
  {
    "name": "Руслан (Ruslan)",
    "video": "Snowman_Ruslan_26"
  },
  {
    "name": "Рустам",
    "video": "Snowman_Рустам_72"
  },
  {
    "name": "Rustam",
    "video": "Snowman_Rustam_72"
  },
  {
    "name": "Саня",
    "video": "Snowman_Саня_39"
  },
  {
    "name": "Саня (Sanya)",
    "video": "Snowman_Sanya_39"
  },
  {
    "name": "Саня (Санька)",
    "video": "Snowman_Санька_39"
  },
  {
    "name": "Саня (Санек)",
    "video": "Snowman_Санек_39"
  },
  {
    "name": "Саня (Санёк)",
    "video": "Snowman_Санёк_39"
  },
  {
    "name": "Саня (Санечка)",
    "video": "Snowman_Санечка_39"
  },
  {
    "name": "Саша",
    "video": "Snowman_Саша_20"
  },
  {
    "name": "Саша (Sasha)",
    "video": "Snowman_Sasha_20"
  },
  {
    "name": "Саша (Сашка)",
    "video": "Snowman_Сашка_20"
  },
  {
    "name": "Саша (Сашок)",
    "video": "Snowman_Сашок_20"
  },
  {
    "name": "Семен",
    "video": "Snowman_Семен_86"
  },
  {
    "name": "Семен (Семён)",
    "video": "Snowman_Семён_86"
  },
  {
    "name": "Сергей",
    "video": "Snowman_Сергей_2"
  },
  {
    "name": "Сергей (Sergei)",
    "video": "Snowman_Sergei_2"
  },
  {
    "name": "Сергей (Sergej)",
    "video": "Snowman_Sergej_2"
  },
  {
    "name": "Сергей (Sergey)",
    "video": "Snowman_Sergey_2"
  },
  {
    "name": "Серега",
    "video": "Snowman_Серега_53"
  },
  {
    "name": "Серега (Серёга)",
    "video": "Snowman_Серёга_53"
  },
  {
    "name": "Серега (Serega)",
    "video": "Snowman_Serega_53"
  },
  {
    "name": "Сережа",
    "video": "Snowman_Сережа_89"
  },
  {
    "name": "Сережа (Серёжа)",
    "video": "Snowman_Серёжа_89"
  },
  {
    "name": "Сережа (Сережка)",
    "video": "Snowman_Сережка_89"
  },
  {
    "name": "Сережа (Серёжка)",
    "video": "Snowman_Серёжка_89"
  },
  {
    "name": "Слава",
    "video": "Snowman_Слава_57"
  },
  {
    "name": "Слава (Slava)",
    "video": "Snowman_Slava_57"
  },
  {
    "name": "Славик",
    "video": "Snowman_Славик_57"
  },
  {
    "name": "Станислав",
    "video": "Snowman_Станислав_45"
  },
  {
    "name": "Станислав (Stanislav)",
    "video": "Snowman_Stanislav_45"
  },
  {
    "name": "Стас",
    "video": "Snowman_Стас_42"
  },
  {
    "name": "Стас (Stas)",
    "video": "Snowman_Stas_42"
  },
  {
    "name": "Стас (Стасик)",
    "video": "Snowman_Стасик_42"
  },
  {
    "name": "Степан",
    "video": "Snowman_Степан_74"
  },
  {
    "name": "Тарас",
    "video": "Snowman_Тарас_73"
  },
  {
    "name": "Taras",
    "video": "Snowman_Taras_73"
  },
  {
    "name": "Тема",
    "video": "Snowman_Тема_80"
  },
  {
    "name": "Тема (Тёма)",
    "video": "Snowman_Тёма_80"
  },
  {
    "name": "Тема (Тёмка)",
    "video": "Snowman_Тёмка_80"
  },
  {
    "name": "Тимофей",
    "video": "Snowman_Тимофей_94"
  },
  {
    "name": "Тимур",
    "video": "Snowman_Тимур_51"
  },
  {
    "name": "Timur",
    "video": "Snowman_Timur_51"
  },
  {
    "name": "Федор",
    "video": "Snowman_Федор_82"
  },
  {
    "name": "Фёдор",
    "video": "Snowman_Фёдор_82"
  },
  {
    "name": "Эдуард",
    "video": "Snowman_Эдуард_58"
  },
  {
    "name": "Эльдар",
    "video": "Snowman_Эльдар_84"
  },
  {
    "name": "Юра",
    "video": "Snowman_Юра_38"
  },
  {
    "name": "Юра (Yura)",
    "video": "Snowman_Yura_38"
  },
  {
    "name": "Юра (Юрик)",
    "video": "Snowman_Юрик_38"
  },
  {
    "name": "Юра (Юрец)",
    "video": "Snowman_Юрец_38"
  },
  {
    "name": "Юра (Юрка)",
    "video": "Snowman_Юрка_38"
  },
  {
    "name": "Юрий",
    "video": "Snowman_Юрий_21"
  },
  {
    "name": "Юрий (Yuriy)",
    "video": "Snowman_Yuriy_21"
  },
  {
    "name": "Юрий (Юрiй)",
    "video": "Snowman_Юрiй_21"
  },
  {
    "name": "Ян",
    "video": "Snowman_Ян_83"
  },
  {
    "name": "Ярослав",
    "video": "Snowman_Ярослав_54"
  },
  {
    "name": "Алёна",
    "video": "Snowballs_Алёна_131"
  },
  {
    "name": "Алёна (Алена)",
    "video": "Snowballs_Алена_131"
  },
  {
    "name": "Алёна (Аленка)",
    "video": "Snowballs_Аленка_131"
  },
  {
    "name": "Алёна (Алёнка)",
    "video": "Snowballs_Алёнка_131"
  },
  {
    "name": "Алёна (Алёночка)",
    "video": "Snowballs_Алёночка_131"
  },
  {
    "name": "Алёна (Алёнушка)",
    "video": "Snowballs_Алёнушка_131"
  },
  {
    "name": "Алёна (Alena)",
    "video": "Snowballs_Alena_131"
  },
  {
    "name": "Анна (Аня)",
    "video": "Snowballs_Аня_121"
  },
  {
    "name": "Анна (Anya)",
    "video": "Snowballs_Anya_121"
  },
  {
    "name": "Анна (Анечка)",
    "video": "Snowballs_Анечка_121"
  },
  {
    "name": "Анна (Анька)",
    "video": "Snowballs_Анька_121"
  },
  {
    "name": "Анна (Анюта)",
    "video": "Snowballs_Анюта_121"
  },
  {
    "name": "Анна (Анютик)",
    "video": "Snowballs_Анютик_121"
  },
  {
    "name": "Анна (Анютка)",
    "video": "Snowballs_Анютка_121"
  },
  {
    "name": "Анна (Анюточка)",
    "video": "Snowballs_Анюточка_121"
  },
  {
    "name": "Валя",
    "video": "Snowballs_Валя_198"
  },
  {
    "name": "Вика",
    "video": "Вика"
  },
  {
    "name": "Вика (Vika)",
    "video": "Vika"
  },
  {
    "name": "Вика (Викуля)",
    "video": "Викуля"
  },
  {
    "name": "Вика (Викусик)",
    "video": "Викусик"
  },
  {
    "name": "Вика (Викуська)",
    "video": "Викуська"
  },
  {
    "name": "Вика (Викуся)",
    "video": "Викуся"
  },
  {
    "name": "Даша",
    "video": "Snowballs_Даша_139"
  },
  {
    "name": "Даша (Dasha)",
    "video": "Snowballs_Dasha_139"
  },
  {
    "name": "Даша (Дашка)",
    "video": "Snowballs_Дашка_139"
  },
  {
    "name": "Даша (Дашенька)",
    "video": "Snowballs_Дашенька_139"
  },
  {
    "name": "Даша (Дашуля)",
    "video": "Snowballs_Дашуля_139"
  },
  {
    "name": "Даша (Дашулька)",
    "video": "Snowballs_Дашулька_139"
  },
  {
    "name": "Даша (Дашуня)",
    "video": "Snowballs_Дашуня_139"
  },
  {
    "name": "Женя (Женечка)",
    "video": "Snowballs_Женечка_190"
  },
  {
    "name": "Ира",
    "video": "Snowballs_Ира_154"
  },
  {
    "name": "Ира (Ipa)",
    "video": "Snowballs_Iра_154"
  },
  {
    "name": "Ира (Ira)",
    "video": "Snowballs_Ira_154"
  },
  {
    "name": "Ира (Ириша)",
    "video": "Snowballs_Ириша_154"
  },
  {
    "name": "Ира (Иришка)",
    "video": "Snowballs_Иришка_154"
  },
  {
    "name": "Ира (Ирка)",
    "video": "Snowballs_Ирка_154"
  },
  {
    "name": "Ира (Ирочка)",
    "video": "Snowballs_Ирочка_154"
  },
  {
    "name": "Катя",
    "video": "Snowballs_Катя_117"
  },
  {
    "name": "Катя (Katya)",
    "video": "Snowballs_Katya_117"
  },
  {
    "name": "Катя (Катенька)",
    "video": "Snowballs_Катенька_117"
  },
  {
    "name": "Катя (Катюня)",
    "video": "Snowballs_Катюня_117"
  },
  {
    "name": "Катя (Катюха)",
    "video": "Snowballs_Катюха_117"
  },
  {
    "name": "Катя (Катюша)",
    "video": "Snowballs_Катюша_117"
  },
  {
    "name": "Катя (Катюшка)",
    "video": "Snowballs_Катюшка_117"
  },
  {
    "name": "Катя (Катька)",
    "video": "Snowballs_Катька_117"
  },
  {
    "name": "Ксюша",
    "video": "Snowballs_Ксюша_155"
  },
  {
    "name": "Ксюша (Ксюня)",
    "video": "Snowballs_Ксюня_155"
  },
  {
    "name": "Ксюша (Ксюха)",
    "video": "Snowballs_Ксюха_155"
  },
  {
    "name": "Ксюша (Ксюшенька)",
    "video": "Snowballs_Ксюшенька_155"
  },
  {
    "name": "Ксюша (Ксюшка)",
    "video": "Snowballs_Ксюшка_155"
  },
  {
    "name": "Лена",
    "video": "Snowballs_Лена_128"
  },
  {
    "name": "Лена (Lena)",
    "video": "Snowballs_Lena_128"
  },
  {
    "name": "Лена (Ленка)",
    "video": "Snowballs_Ленка_128"
  },
  {
    "name": "Лена (Ленок)",
    "video": "Snowballs_Ленок_128"
  },
  {
    "name": "Лена (Леночка)",
    "video": "Snowballs_Леночка_128"
  },
  {
    "name": "Лена (Ленусик)",
    "video": "Snowballs_Ленусик_128"
  },
  {
    "name": "Лена (Ленуська)",
    "video": "Snowballs_Ленуська_128"
  },
  {
    "name": "Лена (Ленуся)",
    "video": "Snowballs_Ленуся_128"
  },
  {
    "name": "Лена (Ленчик)",
    "video": "Snowballs_Ленчик_128"
  },
  {
    "name": "Леся",
    "video": "Snowballs_Леся_172"
  },
  {
    "name": "Лера",
    "video": "Snowballs_Лера_160"
  },
  {
    "name": "Лера (Lera)",
    "video": "Snowballs_Lera_160"
  },
  {
    "name": "Лера (Лерка)",
    "video": "Snowballs_Лерка_160"
  },
  {
    "name": "Лера (Лерочка)",
    "video": "Snowballs_Лерочка_160"
  },
  {
    "name": "Лиза",
    "video": "Snowballs_Лиза_159"
  },
  {
    "name": "Лиза (Liza)",
    "video": "Snowballs_Liza_159"
  },
  {
    "name": "Лиза (Лизочка)",
    "video": "Snowballs_Лизочка_159"
  },
  {
    "name": "Лиля",
    "video": "Snowballs_Лиля_182"
  },
  {
    "name": "Люба",
    "video": "Snowballs_Люба_179"
  },
  {
    "name": "Люба (Любаша)",
    "video": "Snowballs_Любаша_179"
  },
  {
    "name": "Люда",
    "video": "Snowballs_Люда_184"
  },
  {
    "name": "Марина",
    "video": "Snowballs_Марина_111"
  },
  {
    "name": "Марина (Marina)",
    "video": "Snowballs_Marina_111"
  },
  {
    "name": "Марина (Маринка)",
    "video": "Snowballs_Маринка_111"
  },
  {
    "name": "Марина (Мариночка)",
    "video": "Snowballs_Мариночка_111"
  },
  {
    "name": "Марина (Мариша)",
    "video": "Snowballs_Мариша_111"
  },
  {
    "name": "Марина (Маришка)",
    "video": "Snowballs_Маришка_111"
  },
  {
    "name": "Маша",
    "video": "Snowballs_Маша_133"
  },
  {
    "name": "Маша (Masha)",
    "video": "Snowballs_Masha_133"
  },
  {
    "name": "Маша (Машенька)",
    "video": "Snowballs_Машенька_133"
  },
  {
    "name": "Маша (Машка)",
    "video": "Snowballs_Машка_133"
  },
  {
    "name": "Маша (Машулька)",
    "video": "Snowballs_Машулька_133"
  },
  {
    "name": "Маша (Mашуля)",
    "video": "Snowballs_Машуля_133"
  },
  {
    "name": "Маша (Машуня)",
    "video": "Snowballs_Машуня_133"
  },
  {
    "name": "Надя",
    "video": "Snowballs_Надя_163"
  },
  {
    "name": "Надя (Nadya)",
    "video": "Snowballs_Nadya_163"
  },
  {
    "name": "Надя (Наденька)",
    "video": "Snowballs_Наденька_163"
  },
  {
    "name": "Надя (Надюша)",
    "video": "Snowballs_Надюша_163"
  },
  {
    "name": "Надя (Надюшка)",
    "video": "Snowballs_Надюшка_163"
  },
  {
    "name": "Настя",
    "video": "Snowballs_Настя_127"
  },
  {
    "name": "Настя (Nastya)",
    "video": "Snowballs_Nastya_127"
  },
  {
    "name": "Настя (Настена)",
    "video": "Snowballs_Настена_127"
  },
  {
    "name": "Настя (Настёна)",
    "video": "Snowballs_Настёна_127"
  },
  {
    "name": "Настя (Настенка)",
    "video": "Snowballs_Настенка_127"
  },
  {
    "name": "Настя (Настёнка)",
    "video": "Snowballs_Настёнка_127"
  },
  {
    "name": "Настя (Настенька)",
    "video": "Snowballs_Настенька_127"
  },
  {
    "name": "Настя (Настюха)",
    "video": "Snowballs_Настюха_127"
  },
  {
    "name": "Настя (Настюша)",
    "video": "Snowballs_Настюша_127"
  },
  {
    "name": "Настя (Настюшка)",
    "video": "Snowballs_Настюшка_127"
  },
  {
    "name": "Натали",
    "video": "Snowballs_Натали_164"
  },
  {
    "name": "Натали (Natali)",
    "video": "Snowballs_Natali_164"
  },
  {
    "name": "Натали (Natalie)",
    "video": "Snowballs_Natalie_164"
  },
  {
    "name": "Натали (Nataly)",
    "video": "Snowballs_Nataliy_164"
  },
  {
    "name": "Наташа (Natasha)",
    "video": "Snowballs_Natasha_129"
  },
  {
    "name": "Наташа (Наташенька)",
    "video": "Snowballs_Наташенька_129"
  },
  {
    "name": "Наташа (Наташка)",
    "video": "Snowballs_Наташка_129"
  },
  {
    "name": "Наташа (Натусик)",
    "video": "Snowballs_Натусик_129"
  },
  {
    "name": "Наташа (Натуся)",
    "video": "Snowballs_Натуся_129"
  },
  {
    "name": "Ника",
    "video": "Snowballs_Ника_180"
  },
  {
    "name": "Ника (Nika)",
    "video": "Snowballs_Nika_180"
  },
  {
    "name": "Оля",
    "video": "Snowballs_Оля_120"
  },
  {
    "name": "Оля (Olya)",
    "video": "Snowballs_Olya_120"
  },
  {
    "name": "Оля (Оленька)",
    "video": "Snowballs_Оленька_120"
  },
  {
    "name": "Оля (Олечка)",
    "video": "Snowballs_Олечка_120"
  },
  {
    "name": "Оля (Олька)",
    "video": "Snowballs_Олька_120"
  },
  {
    "name": "Оля (Ольчик)",
    "video": "Snowballs_Ольчик_120"
  },
  {
    "name": "Полина",
    "video": "Snowballs_Полина_145"
  },
  {
    "name": "Полина (Polina)",
    "video": "Snowballs_Polina_145"
  },
  {
    "name": "Полина (Полинка)",
    "video": "Snowballs_Полинка_145"
  },
  {
    "name": "Полина (Полиночка)",
    "video": "Snowballs_Полиночка_145"
  },
  {
    "name": "Полина (Полина)",
    "video": "Snowballs_Поля_145"
  },
  {
    "name": "Рита (Rita)",
    "video": "Snowballs_Рита_178"
  },
  {
    "name": "Рита (Rita)",
    "video": "Snowballs_Rita_178"
  },
  {
    "name": "Саша (Сашенька)",
    "video": "Snowballs_Сашенька_175"
  },
  {
    "name": "Саша (Сашулька)",
    "video": "Snowballs_Сашулька_175"
  },
  {
    "name": "Саша (Сашуля)",
    "video": "Snowballs_Сашуля_175"
  },
  {
    "name": "Света",
    "video": "Snowballs_Света_143"
  },
  {
    "name": "Света (Sveta)",
    "video": "Snowballs_Sveta_143"
  },
  {
    "name": "Света (Светик)",
    "video": "Snowballs_Светик_143"
  },
  {
    "name": "Света (Светка)",
    "video": "Snowballs_Светка_143"
  },
  {
    "name": "Света (Светочка)",
    "video": "Snowballs_Светочка_143"
  },
  {
    "name": "Соня",
    "video": "Snowballs_Соня_186"
  },
  {
    "name": "Соня (Сонечка)",
    "video": "Snowballs_Сонечка_186"
  },
  {
    "name": "Таня",
    "video": "Snowballs_Таня_137"
  },
  {
    "name": "Таня (Tanya)",
    "video": "Snowballs_Tanya_137"
  },
  {
    "name": "Таня (Танечка)",
    "video": "Snowballs_Танечка_137"
  },
  {
    "name": "Таня (Танька)",
    "video": "Snowballs_Танька_137"
  },
  {
    "name": "Таня (Танюха)",
    "video": "Snowballs_Танюха_137"
  },
  {
    "name": "Таня (Танюша)",
    "video": "Snowballs_Танюша_137"
  },
  {
    "name": "Таня (Танюшка)",
    "video": "Snowballs_Танюшка_137"
  },
  {
    "name": "Юля",
    "video": "Snowballs_Юля_125"
  },
  {
    "name": "Юля (Юленька)",
    "video": "Snowballs_Юленька_125"
  },
  {
    "name": "Юля (Юлечка)",
    "video": "Snowballs_Юлечка_125"
  },
  {
    "name": "Юля (Юличка)",
    "video": "Snowballs_Юличка_125"
  },
  {
    "name": "Юля (Юлька)",
    "video": "Snowballs_Юлька_125"
  },
  {
    "name": "Юля (Юльчик)",
    "video": "Snowballs_Юльчик_125"
  },
  {
    "name": "Юля (Юляша)",
    "video": "Snowballs_Юляша_125"
  },
  {
    "name": "Юля (Юляшка)",
    "video": "Snowballs_Юляшка_125"
  },
  {
    "name": "Яна",
    "video": "Snowballs_Яна_130"
  },
  {
    "name": "Яна (Yana)",
    "video": "Snowballs_Yana_130"
  },
  {
    "name": "Яна (Янка)",
    "video": "Snowballs_Янка_130"
  },
  {
    "name": "Яна (Яночка)",
    "video": "Snowballs_Яночка_130"
  }
]
