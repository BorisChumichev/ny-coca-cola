module.exports = 
{
  'Алекс': [ 
    '//r.mradx.net/img/21/79FAB8.mp4', 
    '//r.mradx.net/img/86/A87D08.webm',
    '//r.mradx.net/img/D5/B70EED.ogg'
  ]
  ,'Александер': [ 
    '//r.mradx.net/img/2C/D6DE71.mp4', 
    '//r.mradx.net/img/71/080559.webm',
    '//r.mradx.net/img/1C/F09EE2.ogg'
  ]
  ,'Александр': [ 
    '//r.mradx.net/img/66/44CF97.mp4', 
    '//r.mradx.net/img/96/77373F.webm',
    '//r.mradx.net/img/47/355F3E.ogg'
  ]
  ,'Алексей': [ 
    '//r.mradx.net/img/55/F1F387.mp4', 
    '//r.mradx.net/img/42/FBAAE7.webm',
    '//r.mradx.net/img/83/E61B63.ogg'
  ]
  ,'Альберт': [ 
    '//r.mradx.net/img/09/EC6032.mp4', 
    '//r.mradx.net/img/DD/0D18F5.webm',
    '//r.mradx.net/img/B9/996327.ogg'
  ]
  ,'Анатолий': [ 
    '//r.mradx.net/img/5D/D53B10.mp4', 
    '//r.mradx.net/img/01/F7DADC.webm',
    '//r.mradx.net/img/F6/A0F9B4.ogg'
  ]
  ,'Андрей': [ 
    '//r.mradx.net/img/31/2D90DE.mp4', 
    '//r.mradx.net/img/8F/ACC814.webm',
    '//r.mradx.net/img/1E/89626C.ogg'
  ]
  ,'Антон': [ 
    '//r.mradx.net/img/E6/19CDA3.mp4', 
    '//r.mradx.net/img/4C/80B004.webm',
    '//r.mradx.net/img/C0/ACCFF7.ogg'
  ]
  ,'Аркадий': [ 
    '//r.mradx.net/img/0C/D75CB0.mp4', 
    '//r.mradx.net/img/AB/EF7B52.webm',
    '//r.mradx.net/img/E7/3F8066.ogg'
  ]
  ,'Арсений': [ 
    '//r.mradx.net/img/F5/688BCC.mp4', 
    '//r.mradx.net/img/98/69E96C.webm',
    '//r.mradx.net/img/AE/B44FE2.ogg'
  ]
  ,'Артем': [ 
    '//r.mradx.net/img/65/71601A.mp4', 
    '//r.mradx.net/img/6F/C99AA6.webm',
    '//r.mradx.net/img/0C/238711.ogg'
  ]
  ,'Артём': [ 
    '//r.mradx.net/img/E2/0D3848.mp4', 
    '//r.mradx.net/img/51/F32B96.webm',
    '//r.mradx.net/img/47/4BC5A1.ogg'
  ]
  ,'Артёмка': [ 
    '//r.mradx.net/img/9C/9C8204.mp4', 
    '//r.mradx.net/img/91/808A1A.webm',
    '//r.mradx.net/img/AE/264406.ogg'
  ]
  ,'Артур': [ 
    '//r.mradx.net/img/18/EED99D.mp4', 
    '//r.mradx.net/img/0B/D0808D.webm',
    '//r.mradx.net/img/27/CF9E08.ogg'
  ]
  ,'Богдан': [ 
    '//r.mradx.net/img/04/34931D.mp4', 
    '//r.mradx.net/img/0A/B559F2.webm',
    '//r.mradx.net/img/CC/0D6C3D.ogg'
  ]
  ,'Борис': [ 
    '//r.mradx.net/img/B1/DAC469.mp4', 
    '//r.mradx.net/img/E5/AAF2DC.webm',
    '//r.mradx.net/img/DD/17F2B0.ogg'
  ]
  ,'Вадим': [ 
    '//r.mradx.net/img/E1/B87FC2.mp4', 
    '//r.mradx.net/img/DE/9EB5B1.webm',
    '//r.mradx.net/img/8B/454E20.ogg'
  ]
  ,'Валентин': [ 
    '//r.mradx.net/img/40/3C867C.mp4', 
    '//r.mradx.net/img/F8/94AA1E.webm',
    '//r.mradx.net/img/E1/BAA668.ogg'
  ]
  ,'Валера': [ 
    '//r.mradx.net/img/C7/A66A25.mp4', 
    '//r.mradx.net/img/FF/BD8279.webm',
    '//r.mradx.net/img/D6/255F4A.ogg'
  ]
  ,'Валерий': [ 
    '//r.mradx.net/img/83/E24322.mp4', 
    '//r.mradx.net/img/AF/92FEA0.webm',
    '//r.mradx.net/img/E3/D62230.ogg'
  ]
  ,'Ваня': [ 
    '//r.mradx.net/img/7D/FDF8BE.mp4', 
    '//r.mradx.net/img/7B/4BF680.webm',
    '//r.mradx.net/img/F3/BB62CF.ogg'
  ]
  ,'Василий': [ 
    '//r.mradx.net/img/3E/DC3F7A.mp4', 
    '//r.mradx.net/img/16/80E24A.webm',
    '//r.mradx.net/img/3A/AE2404.ogg'
  ]
  ,'Вася': [ 
    '//r.mradx.net/img/4C/1B3BF4.mp4', 
    '//r.mradx.net/img/69/472202.webm',
    '//r.mradx.net/img/15/992FA9.ogg'
  ]
  ,'Виктор': [ 
    '//r.mradx.net/img/12/E3345E.mp4', 
    '//r.mradx.net/img/00/B50F06.webm',
    '//r.mradx.net/img/1C/177262.ogg'
  ]
  ,'Витя': [ 
    '//r.mradx.net/img/12/E3345E.mp4', 
    '//r.mradx.net/img/00/B50F06.webm',
    '//r.mradx.net/img/1C/177262.ogg'
  ]
  ,'Виталий': [ 
    '//r.mradx.net/img/72/7CCC26.mp4', 
    '//r.mradx.net/img/F4/28C28F.webm',
    '//r.mradx.net/img/67/26968A.ogg'
  ]
  ,'Виталик': [ 
    '//r.mradx.net/img/C4/D79B7D.mp4', 
    '//r.mradx.net/img/79/329445.webm',
    '//r.mradx.net/img/CF/263B36.ogg'
  ]
  ,'Влад': [ 
    '//r.mradx.net/img/D6/570E37.mp4', 
    '//r.mradx.net/img/80/79B191.webm',
    '//r.mradx.net/img/51/994673.ogg'
  ]
  ,'Владислав': [ 
    '//r.mradx.net/img/23/545307.mp4', 
    '//r.mradx.net/img/D7/10A3CA.webm',
    '//r.mradx.net/img/31/00554E.ogg'
  ]
  ,'Владимир': [ 
    '//r.mradx.net/img/D9/4740EE.mp4', 
    '//r.mradx.net/img/A1/064179.webm',
    '//r.mradx.net/img/BC/94F80B.ogg'
  ]
  ,'Вова': [ 
    '//r.mradx.net/img/87/69D5D2.mp4', 
    '//r.mradx.net/img/64/7D67B6.webm',
    '//r.mradx.net/img/55/24CB40.ogg'
  ]
  ,'Володя': [ 
    '//r.mradx.net/img/27/6FF80A.mp4', 
    '//r.mradx.net/img/DF/E2AB67.webm',
    '//r.mradx.net/img/FA/CED3BD.ogg'
  ]
  ,'Вячеслав': [ 
    '//r.mradx.net/img/69/447F86.mp4', 
    '//r.mradx.net/img/5F/FE2AD3.webm',
    '//r.mradx.net/img/1B/05357A.ogg'
  ]
  ,'Гена': [ 
    '//r.mradx.net/img/BA/0F5833.mp4', 
    '//r.mradx.net/img/EC/6B84B8.webm',
    '//r.mradx.net/img/23/8D74B8.ogg'
  ]
  ,'Генадий': [ 
    '//r.mradx.net/img/13/B3A041.mp4', 
    '//r.mradx.net/img/DD/F54158.webm',
    '//r.mradx.net/img/41/9282E5.ogg'
  ]
  ,'Геннадий': [ 
    '//r.mradx.net/img/A5/0F288E.mp4', 
    '//r.mradx.net/img/66/95A0FA.webm',
    '//r.mradx.net/img/24/425A4C.ogg'
  ]
  ,'Георгий': [ 
    '//r.mradx.net/img/6F/725915.mp4', 
    '//r.mradx.net/img/0B/3D415A.webm',
    '//r.mradx.net/img/02/944F2A.ogg'
  ]
  ,'Глеб': [ 
    '//r.mradx.net/img/D5/B40677.mp4', 
    '//r.mradx.net/img/C2/FFF3FC.webm',
    '//r.mradx.net/img/9D/7281DF.ogg'
  ]
  ,'Григорий': [ 
    '//r.mradx.net/img/80/4B1595.mp4', 
    '//r.mradx.net/img/DC/FF752C.webm',
    '//r.mradx.net/img/CA/9C5F97.ogg'
  ]
  ,'Давид': [ 
    '//r.mradx.net/img/B6/7C614F.mp4', 
    '//r.mradx.net/img/36/8CBE7A.webm',
    '//r.mradx.net/img/9D/20EC6C.ogg'
  ]
  ,'Дамир': [ 
    '//r.mradx.net/img/78/37161D.mp4', 
    '//r.mradx.net/img/35/4E4CE5.webm',
    '//r.mradx.net/img/30/75FFCB.ogg'
  ]
  ,'Даниил': [ 
    '//r.mradx.net/img/DA/485685.mp4', 
    '//r.mradx.net/img/27/569FA1.webm',
    '//r.mradx.net/img/37/C4BB20.ogg'
  ]
  ,'Данил': [ 
    '//r.mradx.net/img/B9/2EFC11.mp4', 
    '//r.mradx.net/img/D2/AE11D8.webm',
    '//r.mradx.net/img/91/F51484.ogg'
  ]
  ,'Данила': [ 
    '//r.mradx.net/img/01/81D6D0.mp4', 
    '//r.mradx.net/img/BB/593A0E.webm',
    '//r.mradx.net/img/3F/D458DD.ogg'
  ]
  ,'Денис': [ 
    '//r.mradx.net/img/A4/4490DF.mp4', 
    '//r.mradx.net/img/CA/A4A9CB.webm',
    '//r.mradx.net/img/62/C2341E.ogg'
  ]
  ,'Дима': [ 
    '//r.mradx.net/img/81/636DEF.mp4', 
    '//r.mradx.net/img/26/C97A5E.webm',
    '//r.mradx.net/img/9D/F2BF61.ogg'
  ]
  ,'Дмитрий': [ 
    '//r.mradx.net/img/28/17970E.mp4', 
    '//r.mradx.net/img/4C/B4F3F5.webm',
    '//r.mradx.net/img/EB/8D2F43.ogg'
  ]
  ,'Дружище': [ 
    '//r.mradx.net/img/EB/4E8788.mp4', 
    '//r.mradx.net/img/F0/6D82D3.webm',
    '//r.mradx.net/img/6A/4A4808.ogg'
  ]
  ,'Евгений': [ 
    '//r.mradx.net/img/D2/D70F46.mp4', 
    '//r.mradx.net/img/A2/2E5184.webm',
    '//r.mradx.net/img/45/555CDD.ogg'
  ]
  ,'Егор': [ 
    '//r.mradx.net/img/2C/A16207.mp4', 
    '//r.mradx.net/img/55/57E0FA.webm',
    '//r.mradx.net/img/4A/267033.ogg'
  ]
  ,'Игорь': [ 
    '//r.mradx.net/img/AC/211C48.mp4', 
    '//r.mradx.net/img/4C/8F4B1A.webm',
    '//r.mradx.net/img/E6/BEEA7F.ogg'
  ]
  ,'Иван': [ 
    '//r.mradx.net/img/9E/FE0D2C.mp4', 
    '//r.mradx.net/img/D4/EA8995.webm',
    '//r.mradx.net/img/DC/0857F8.ogg'
  ]
  ,'Женя': [ 
    '//r.mradx.net/img/F9/32F931.mp4', 
    '//r.mradx.net/img/38/B31BEF.webm',
    '//r.mradx.net/img/F0/16BCF4.ogg'
  ]
  ,'Ильдар': [ 
    '//r.mradx.net/img/6D/6C63BB.mp4', 
    '//r.mradx.net/img/BB/FF3D76.webm',
    '//r.mradx.net/img/82/6B3116.ogg'
  ]
  ,'Илья': [ 
    '//r.mradx.net/img/83/11EF8B.mp4', 
    '//r.mradx.net/img/E9/10EA4D.webm',
    '//r.mradx.net/img/57/9AF0EE.ogg'
  ]
  ,'Кирил': [ 
    '//r.mradx.net/img/CE/7F2071.mp4', 
    '//r.mradx.net/img/84/27C798.webm',
    '//r.mradx.net/img/63/ABA3D7.ogg'
  ]
  ,'Кирилл': [ 
    '//r.mradx.net/img/CE/7F2071.mp4', 
    '//r.mradx.net/img/4C/E094B1.webm',
    '//r.mradx.net/img/EF/BAB908.ogg'
  ]
  ,'Коля': [ 
    '//r.mradx.net/img/66/574473.mp4', 
    '//r.mradx.net/img/69/870775.webm',
    '//r.mradx.net/img/9E/CC131B.ogg'
  ]
  ,'Константин': [ 
    '//r.mradx.net/img/61/3B0A9A.mp4', 
    '//r.mradx.net/img/F8/DF1E79.webm',
    '//r.mradx.net/img/78/6CF0B4.ogg'
  ]
  ,'Костя': [ 
    '//r.mradx.net/img/F4/11BD05.mp4', 
    '//r.mradx.net/img/D0/511A0A.webm',
    '//r.mradx.net/img/5B/627ADC.ogg'
  ]
  ,'Олександр': [ 
    '//r.mradx.net/img/1F/FADAB9.mp4', 
    '//r.mradx.net/img/A2/AA63EC.webm',
    '//r.mradx.net/img/83/0E4350.ogg'
  ]
  ,'Павел': [ 
    '//r.mradx.net/img/D0/44C692.mp4', 
    '//r.mradx.net/img/4E/A48C5C.webm',
    '//r.mradx.net/img/4C/A4F0DD.ogg'
  ]
  ,'Паша': [ 
    '//r.mradx.net/img/DD/022418.mp4', 
    '//r.mradx.net/img/E2/64E442.webm',
    '//r.mradx.net/img/36/0D1E59.ogg'
  ]
  ,'Петр': [ 
    '//r.mradx.net/img/94/60DB92.mp4', 
    '//r.mradx.net/img/4A/FCFBC4.webm',
    '//r.mradx.net/img/F0/4845A0.ogg'
  ]
  ,'Пётр': [ 
    '//r.mradx.net/img/11/E26B78.mp4', 
    '//r.mradx.net/img/D1/2FD98A.webm',
    '//r.mradx.net/img/33/CF3E50.ogg'
  ]
  ,'Петя': [ 
    '//r.mradx.net/img/ED/C23427.mp4', 
    '//r.mradx.net/img/A5/EFE23C.webm',
    '//r.mradx.net/img/ED/BAF231.ogg'
  ]
  ,'Рамиль': [ 
    '//r.mradx.net/img/DA/2D2F12.mp4', 
    '//r.mradx.net/img/60/E66E16.webm',
    '//r.mradx.net/img/85/31DF1C.ogg'
  ]
  ,'Ренат': [ 
    '//r.mradx.net/img/ED/BAF231.ogg', 
    '//r.mradx.net/img/AF/2D6FDA.webm',
    '//r.mradx.net/img/59/6D321A.ogg'
  ]
  ,'Ринат': [ 
    '//r.mradx.net/img/0F/B3ED5D.mp4', 
    '//r.mradx.net/img/7E/92A792.webm',
    '//r.mradx.net/img/E7/EAF379.ogg'
  ]
  ,'Рома': [ 
    '//r.mradx.net/img/37/E10B36.mp4', 
    '//r.mradx.net/img/8C/77055B.webm',
    '//r.mradx.net/img/B5/EFF812.ogg'
  ]
  ,'Роман': [ 
    '//r.mradx.net/img/26/237B04.mp4', 
    '//r.mradx.net/img/68/7C5CB1.webm',
    '//r.mradx.net/img/7D/19848E.ogg'
  ]
  ,'Руслан': [ 
    '//r.mradx.net/img/F5/E8EE9F.mp4', 
    '//r.mradx.net/img/62/06C357.webm',
    '//r.mradx.net/img/13/123C85.ogg'
  ]
  ,'Рустам': [ 
    '//r.mradx.net/img/B9/30B01E.mp4', 
    '//r.mradx.net/img/65/73AED2.webm',
    '//r.mradx.net/img/CE/CFFAB1.ogg'
  ]
  ,'Саня': [ 
    '//r.mradx.net/img/66/AA2830.mp4', 
    '//r.mradx.net/img/20/C44961.webm',
    '//r.mradx.net/img/E4/54FDCA.ogg'
  ]
  ,'Саша': [ 
    '//r.mradx.net/img/CE/169C1E.mp4', 
    '//r.mradx.net/img/F2/8C0095.webm',
    '//r.mradx.net/img/25/BD0643.ogg'
  ]
  ,'Семен': [ 
    '//r.mradx.net/img/25/BD0643.ogg', 
    '//r.mradx.net/img/66/7F59E8.webm',
    '//r.mradx.net/img/BB/538FF5.ogg'
  ]
  ,'Сергей': [ 
    '//r.mradx.net/img/BB/BB0D10.mp4', 
    '//r.mradx.net/img/6A/FA2C78.webm',
    '//r.mradx.net/img/B9/BE118A.ogg'
  ]
  ,'Серега': [ 
    '//r.mradx.net/img/60/FD17DA.mp4', 
    '//r.mradx.net/img/FB/D9CC0A.webm',
    '//r.mradx.net/img/E3/85FBB7.ogg'
  ]
  ,'Сережа': [ 
    '//r.mradx.net/img/16/088FE3.mp4', 
    '//r.mradx.net/img/6F/D23621.webm',
    '//r.mradx.net/img/48/5BF57F.ogg'
  ]
  ,'Слава': [ 
    '//r.mradx.net/img/47/69F303.mp4', 
    '//r.mradx.net/img/51/59268E.webm',
    '//r.mradx.net/img/F8/434A6B.ogg'
  ]
  ,'Станислав': [ 
    '//r.mradx.net/img/8C/E936A9.mp4', 
    '//r.mradx.net/img/81/FECB98.webm',
    '//r.mradx.net/img/50/D6920D.ogg'
  ]
  ,'Алёна': [ 
    '//r.mradx.net/img/F7/48ADDE.mp4', 
    '//r.mradx.net/img/1C/F171B7.webm',
    '//r.mradx.net/img/85/2657A4.ogg'
  ]
  ,'Валя': [ 
    '//r.mradx.net/img/92/61163C.mp4', 
    '//r.mradx.net/img/34/0B967F.webm',
    '//r.mradx.net/img/BE/8D0D1A.ogg'
  ]
  ,'Даша': [ 
    '//r.mradx.net/img/63/95F0E5.mp4', 
    '//r.mradx.net/img/ED/FA91FE.webm',
    '//r.mradx.net/img/80/EE857F.ogg'
  ]
  ,'Ира': [ 
    '//r.mradx.net/img/0F/7EBE4E.mp4', 
    '//r.mradx.net/img/BE/D9A881.webm',
    '//r.mradx.net/img/7C/C22A5B.ogg'
  ]
  ,'Катя': [ 
    '//r.mradx.net/img/68/04BF8D.mp4', 
    '//r.mradx.net/img/F4/C8D5DC.webm',
    '//r.mradx.net/img/FA/B845E3.ogg'
  ]
  ,'Ксюша': [ 
    '//r.mradx.net/img/67/04EB1A.mp4', 
    '//r.mradx.net/img/96/92FB73.webm',
    '//r.mradx.net/img/9A/86BA37.ogg'
  ]
  ,'Лена': [ 
    '//r.mradx.net/img/D7/C9F2C7.mp4', 
    '//r.mradx.net/img/3B/0D109A.webm',
    '//r.mradx.net/img/96/C636A6.ogg'
  ]
  ,'Леся': [ 
    '//r.mradx.net/img/ED/2DD66D.mp4', 
    '//r.mradx.net/img/02/4B3FCE.webm',
    '//r.mradx.net/img/64/88C396.ogg'
  ]
  ,'Лера': [ 
    '//r.mradx.net/img/42/4DEF1C.mp4', 
    '//r.mradx.net/img/2A/0BAF9A.webm',
    '//r.mradx.net/img/8D/13B14C.ogg'
  ]
  ,'Лиза': [ 
    '//r.mradx.net/img/14/7A90CC.mp4', 
    '//r.mradx.net/img/53/7DE49A.webm',
    '//r.mradx.net/img/98/3585B5.ogg'
  ]
  ,'Лиля': [ 
    '//r.mradx.net/img/AC/6AFA33.mp4', 
    '//r.mradx.net/img/8F/BE874E.webm',
    '//r.mradx.net/img/62/4A261A.ogg'
  ]
  ,'Люба': [ 
    '//r.mradx.net/img/B3/88EDD5.mp4', 
    '//r.mradx.net/img/0A/EFBC25.webm',
    '//r.mradx.net/img/DD/698F9E.ogg'
  ]
  ,'Люда': [ 
    '//r.mradx.net/img/48/B380A2.mp4', 
    '//r.mradx.net/img/DB/E63F1A.webm',
    '//r.mradx.net/img/BA/82EBA8.ogg'
  ]
  ,'Марина': [ 
    '//r.mradx.net/img/66/14AF5D.mp4', 
    '//r.mradx.net/img/1B/212580.webm',
    '//r.mradx.net/img/19/1496B4.ogg'
  ]
  ,'Маша': [ 
    '//r.mradx.net/img/9B/B03344.mp4', 
    '//r.mradx.net/img/C0/45CEC1.webm',
    '//r.mradx.net/img/22/6D47B0.ogg'
  ]
  ,'Надя': [ 
    '//r.mradx.net/img/EB/818E5F.mp4', 
    '//r.mradx.net/img/CB/C9E936.webm',
    '//r.mradx.net/img/A5/E9388B.ogg'
  ]
  ,'Настя': [ 
    '//r.mradx.net/img/8B/81EFD2.mp4', 
    '//r.mradx.net/img/DC/6A7F4E.webm',
    '//r.mradx.net/img/BD/27186A.ogg'
  ]
  ,'Натали': [ 
    '//r.mradx.net/img/66/193326.mp4', 
    '//r.mradx.net/img/FC/A591BD.webm',
    '//r.mradx.net/img/6D/5BC532.ogg'
  ]
  ,'Ника': [ 
    '//r.mradx.net/img/6D/FDBD65.mp4', 
    '//r.mradx.net/img/0D/51D990.webm',
    '//r.mradx.net/img/61/04F623.ogg'
  ]
  ,'Оля': [ 
    '//r.mradx.net/img/EE/AE48EA.mp4', 
    '//r.mradx.net/img/03/DF7279.webm',
    '//r.mradx.net/img/AA/08091C.ogg'
  ]
  ,'Полина': [ 
    '//r.mradx.net/img/42/828226.mp4', 
    '//r.mradx.net/img/69/7436DC.webm',
    '//r.mradx.net/img/15/012AAE.ogg'
  ]
  ,'Света': [ 
    '//r.mradx.net/img/72/755B86.mp4', 
    '//r.mradx.net/img/6A/24DEB0.webm',
    '//r.mradx.net/img/C2/E8B687.ogg'
  ]
  ,'Соня': [ 
    '//r.mradx.net/img/6B/FD73A1.mp4', 
    '//r.mradx.net/img/CD/A27F99.webm',
    '//r.mradx.net/img/4A/804424.ogg'
  ]
  ,'Таня': [ 
    '//r.mradx.net/img/EB/DDDB67.mp4', 
    '//r.mradx.net/img/73/F834E6.webm',
    '//r.mradx.net/img/0C/D27DEC.ogg'
  ]
  ,'Юля': [ 
    '//r.mradx.net/img/C5/FE03CF.mp4', 
    '//r.mradx.net/img/D9/DF6B1D.webm',
    '//r.mradx.net/img/8D/10794D.ogg'
  ]
  ,'Яна': [ 
    '//r.mradx.net/img/A9/914FF8.mp4', 
    '//r.mradx.net/img/A1/EAA42A.webm',
    '//r.mradx.net/img/31/FFA7F6.ogg'
  ]
  ,'Айгуль': [ 
    '//r.mradx.net/img/A3/1DB279.mp4', 
    '//r.mradx.net/img/AE/3109E6.webm',
    '//r.mradx.net/img/1B/D631A5.ogg'
  ]
  ,'Алina': [ 
    '//r.mradx.net/img/A3/FDA528.mp4', 
    '//r.mradx.net/img/A7/438D90.webm',
    '//r.mradx.net/img/E3/848B1D.ogg'
  ]
  ,'Алина': [ 
    '//r.mradx.net/img/E5/BDE8E3.mp4', 
    '//r.mradx.net/img/00/21F52E.webm',
    '//r.mradx.net/img/1A/B3C6E0.ogg'
  ]
  ,'Алиса': [ 
    '//r.mradx.net/img/0B/AA37F6.mp4', 
    '//r.mradx.net/img/A6/8D2C62.webm',
    '//r.mradx.net/img/43/82E5A7.ogg'
  ]
  ,'Алия': [ 
    '//r.mradx.net/img/C2/A9B8EB.mp4', 
    '//r.mradx.net/img/0C/406490.webm',
    '//r.mradx.net/img/6F/69B64C.ogg'
  ]
  ,'Алла': [ 
    '//r.mradx.net/img/5A/581565.mp4', 
    '//r.mradx.net/img/98/C94834.webm',
    '//r.mradx.net/img/A8/45F93F.ogg'
  ]
  ,'Альбина': [ 
    '//r.mradx.net/img/D3/7C77C8.mp4', 
    '//r.mradx.net/img/70/8B0F2C.webm',
    '//r.mradx.net/img/B8/5FCD04.ogg'
  ]
  ,'Ангелина': [ 
    '//r.mradx.net/img/E3/258D27.mp4', 
    '//r.mradx.net/img/18/D4455D.webm',
    '//r.mradx.net/img/65/ACC86D.ogg'
  ]
  ,'Анжела': [ 
    '//r.mradx.net/img/1C/6630B0.mp4', 
    '//r.mradx.net/img/CC/EC771E.webm',
    '//r.mradx.net/img/E5/7FE7B6.ogg'
  ]
  ,'Анжелика': [ 
    '//r.mradx.net/img/12/8F428E.mp4', 
    '//r.mradx.net/img/A6/99C3EA.webm',
    '//r.mradx.net/img/A0/E3E3B0.ogg'
  ]
  ,'Арина': [ 
    '//r.mradx.net/img/48/9613B5.mp4', 
    '//r.mradx.net/img/97/377087.webm',
    '//r.mradx.net/img/3B/A92B07.ogg'
  ]
  ,'Ася': [ 
    '//r.mradx.net/img/FD/6B08C5.mp4', 
    '//r.mradx.net/img/D6/336190.webm',
    '//r.mradx.net/img/A7/662D26.ogg'
  ]
  ,'Вера': [ 
    '//r.mradx.net/img/05/F845BF.mp4', 
    '//r.mradx.net/img/C6/EF7FE8.webm',
    '//r.mradx.net/img/98/051308.ogg'
  ]
  ,'Вероника': [ 
    '//r.mradx.net/img/02/80564C.mp4', 
    '//r.mradx.net/img/B4/F04C9A.webm',
    '//r.mradx.net/img/10/397AD7.ogg'
  ]
  ,'Галя': [ 
    '//r.mradx.net/img/6F/EC3143.mp4', 
    '//r.mradx.net/img/85/7AD9A3.webm',
    '//r.mradx.net/img/01/25327D.ogg'
  ]
  ,'Гузель': [ 
    '//r.mradx.net/img/38/A34BCF.mp4', 
    '//r.mradx.net/img/89/F8D1A0.webm',
    '//r.mradx.net/img/78/E75ECC.ogg'
  ]
  ,'Гульнара': [ 
    '//r.mradx.net/img/24/0BCB55.mp4', 
    '//r.mradx.net/img/36/6FCCA5.webm',
    '//r.mradx.net/img/56/F901AC.ogg'
  ]
  ,'Диана': [ 
    '//r.mradx.net/img/ED/B4D1C7.mp4', 
    '//r.mradx.net/img/53/A38A97.webm',
    '//r.mradx.net/img/3C/56538B.ogg'
  ]
  ,'Дина': [ 
    '//r.mradx.net/img/7D/ED8A0D.mp4', 
    '//r.mradx.net/img/06/F4A081.webm',
    '//r.mradx.net/img/D4/6866C0.ogg'
  ]
  ,'Динара': [ 
    '//r.mradx.net/img/80/B6F556.mp4', 
    '//r.mradx.net/img/8A/61DE4A.webm',
    '//r.mradx.net/img/C5/692114.ogg'
  ]
  ,'Жанна': [ 
    '//r.mradx.net/img/95/A50742.mp4', 
    '//r.mradx.net/img/0A/01964E.webm',
    '//r.mradx.net/img/92/A7F5AD.ogg'
  ]
  ,'Зоя': [ 
    '//r.mradx.net/img/EC/8B4B76.mp4', 
    '//r.mradx.net/img/C3/73F17C.webm',
    '//r.mradx.net/img/AC/44F18E.ogg'
  ]
  ,'Илона': [ 
    '//r.mradx.net/img/7A/B927FC.mp4', 
    '//r.mradx.net/img/E8/A5DA77.webm',
    '//r.mradx.net/img/EC/86CC6D.ogg'
  ]
  ,'Инга': [ 
    '//r.mradx.net/img/19/B36C68.mp4', 
    '//r.mradx.net/img/AD/FBA2C5.webm',
    '//r.mradx.net/img/DF/953802.ogg'
  ]
  ,'Карина': [ 
    '//r.mradx.net/img/FB/B444BB.mp4', 
    '//r.mradx.net/img/90/8A0E09.webm',
    '//r.mradx.net/img/42/C26BCB.ogg'
  ]
  ,'Кира': [ 
    '//r.mradx.net/img/E1/85F867.mp4', 
    '//r.mradx.net/img/2C/569575.webm',
    '//r.mradx.net/img/1E/51700A.ogg'
  ]
  ,'Лариса': [ 
    '//r.mradx.net/img/F0/9A941C.mp4', 
    '//r.mradx.net/img/D9/FEAB95.webm',
    '//r.mradx.net/img/52/FFAB05.ogg'
  ]
  ,'Лилия': [ 
    '//r.mradx.net/img/78/894B7B.mp4', 
    '//r.mradx.net/img/48/14F937.webm',
    '//r.mradx.net/img/A7/206EDB.ogg'
  ]
  ,'Любовь': [ 
    '//r.mradx.net/img/5C/B989AD.mp4', 
    '//r.mradx.net/img/84/2213AD.webm',
    '//r.mradx.net/img/39/7AB149.ogg'
  ]
  ,'Майя': [ 
    '//r.mradx.net/img/DC/5EBC65.mp4', 
    '//r.mradx.net/img/25/019791.webm',
    '//r.mradx.net/img/F3/182F2F.ogg'
  ]
  ,'Нина': [ 
    '//r.mradx.net/img/CA/CDF131.mp4', 
    '//r.mradx.net/img/72/4E7A93.webm',
    '//r.mradx.net/img/CF/222FF4.ogg'
  ]
  ,'Регина': [ 
    '//r.mradx.net/img/F1/40246A.mp4', 
    '//r.mradx.net/img/63/B954AA.webm',
    '//r.mradx.net/img/5A/134127.ogg'
  ]
  ,'Тамара': [ 
    '//r.mradx.net/img/33/DBDB85.mp4', 
    '//r.mradx.net/img/84/4712A5.webm',
    '//r.mradx.net/img/17/24425F.ogg'
  ]
  ,'Ульяна': [ 
    '//r.mradx.net/img/A9/0B3894.mp4', 
    '//r.mradx.net/img/8C/204931.webm',
    '//r.mradx.net/img/04/89B68C.ogg'
  ]
  ,'Элина': [ 
    '//r.mradx.net/img/6E/893E61.mp4', 
    '//r.mradx.net/img/6C/60C197.webm',
    '//r.mradx.net/img/34/CAF4DC.ogg'
  ]
  ,'Эльвира': [ 
    '//r.mradx.net/img/D3/4E5D28.mp4', 
    '//r.mradx.net/img/DB/3C6EC0.webm',
    '//r.mradx.net/img/2F/F0CEB0.ogg'
  ]
  ,'Алекс': [ 
    '//r.mradx.net/img/21/79FAB8.mp4', 
    '//r.mradx.net/img/86/A87D08.webm',
    '//r.mradx.net/img/D5/B70EED.ogg'
  ]
  ,'Александер': [ 
    '//r.mradx.net/img/2C/D6DE71.mp4', 
    '//r.mradx.net/img/71/080559.webm',
    '//r.mradx.net/img/1C/F09EE2.ogg'
  ]
  ,'Александр': [ 
    '//r.mradx.net/img/66/44CF97.mp4', 
    '//r.mradx.net/img/96/77373F.webm',
    '//r.mradx.net/img/47/355F3E.ogg'
  ]
  ,'Алексей': [ 
    '//r.mradx.net/img/55/F1F387.mp4', 
    '//r.mradx.net/img/42/FBAAE7.webm',
    '//r.mradx.net/img/83/E61B63.ogg'
  ]
  ,'Альберт': [ 
    '//r.mradx.net/img/09/EC6032.mp4', 
    '//r.mradx.net/img/DD/0D18F5.webm',
    '//r.mradx.net/img/B9/996327.ogg'
  ]
  ,'Анатолий': [ 
    '//r.mradx.net/img/5D/D53B10.mp4', 
    '//r.mradx.net/img/01/F7DADC.webm',
    '//r.mradx.net/img/F6/A0F9B4.ogg'
  ]
  ,'Андрей': [ 
    '//r.mradx.net/img/31/2D90DE.mp4', 
    '//r.mradx.net/img/8F/ACC814.webm',
    '//r.mradx.net/img/1E/89626C.ogg'
  ]
  ,'Антон': [ 
    '//r.mradx.net/img/E6/19CDA3.mp4', 
    '//r.mradx.net/img/4C/80B004.webm',
    '//r.mradx.net/img/C0/ACCFF7.ogg'
  ]
  ,'Аркадий': [ 
    '//r.mradx.net/img/0C/D75CB0.mp4', 
    '//r.mradx.net/img/AB/EF7B52.webm',
    '//r.mradx.net/img/E7/3F8066.ogg'
  ]
  ,'Арсений': [ 
    '//r.mradx.net/img/F5/688BCC.mp4', 
    '//r.mradx.net/img/98/69E96C.webm',
    '//r.mradx.net/img/AE/B44FE2.ogg'
  ]
  ,'Артем': [ 
    '//r.mradx.net/img/65/71601A.mp4', 
    '//r.mradx.net/img/6F/C99AA6.webm',
    '//r.mradx.net/img/0C/238711.ogg'
  ]
  ,'Артём': [ 
    '//r.mradx.net/img/E2/0D3848.mp4', 
    '//r.mradx.net/img/51/F32B96.webm',
    '//r.mradx.net/img/47/4BC5A1.ogg'
  ]
  ,'Артёмка': [ 
    '//r.mradx.net/img/9C/9C8204.mp4', 
    '//r.mradx.net/img/91/808A1A.webm',
    '//r.mradx.net/img/AE/264406.ogg'
  ]
  ,'Артур': [ 
    '//r.mradx.net/img/18/EED99D.mp4', 
    '//r.mradx.net/img/0B/D0808D.webm',
    '//r.mradx.net/img/27/CF9E08.ogg'
  ]
  ,'Богдан': [ 
    '//r.mradx.net/img/04/34931D.mp4', 
    '//r.mradx.net/img/0A/B559F2.webm',
    '//r.mradx.net/img/CC/0D6C3D.ogg'
  ]
  ,'Борис': [ 
    '//r.mradx.net/img/B1/DAC469.mp4', 
    '//r.mradx.net/img/E5/AAF2DC.webm',
    '//r.mradx.net/img/DD/17F2B0.ogg'
  ]
  ,'Вадим': [ 
    '//r.mradx.net/img/E1/B87FC2.mp4', 
    '//r.mradx.net/img/DE/9EB5B1.webm',
    '//r.mradx.net/img/8B/454E20.ogg'
  ]
  ,'Валентин': [ 
    '//r.mradx.net/img/40/3C867C.mp4', 
    '//r.mradx.net/img/F8/94AA1E.webm',
    '//r.mradx.net/img/E1/BAA668.ogg'
  ]
  ,'Валера': [ 
    '//r.mradx.net/img/C7/A66A25.mp4', 
    '//r.mradx.net/img/FF/BD8279.webm',
    '//r.mradx.net/img/D6/255F4A.ogg'
  ]
  ,'Валерий': [ 
    '//r.mradx.net/img/83/E24322.mp4', 
    '//r.mradx.net/img/AF/92FEA0.webm',
    '//r.mradx.net/img/E3/D62230.ogg'
  ]
  ,'Ваня': [ 
    '//r.mradx.net/img/7D/FDF8BE.mp4', 
    '//r.mradx.net/img/7B/4BF680.webm',
    '//r.mradx.net/img/F3/BB62CF.ogg'
  ]
  ,'Василий': [ 
    '//r.mradx.net/img/3E/DC3F7A.mp4', 
    '//r.mradx.net/img/16/80E24A.webm',
    '//r.mradx.net/img/3A/AE2404.ogg'
  ]
  ,'Вася': [ 
    '//r.mradx.net/img/4C/1B3BF4.mp4', 
    '//r.mradx.net/img/69/472202.webm',
    '//r.mradx.net/img/15/992FA9.ogg'
  ]
  ,'Виктор': [ 
    '//r.mradx.net/img/12/E3345E.mp4', 
    '//r.mradx.net/img/00/B50F06.webm',
    '//r.mradx.net/img/1C/177262.ogg'
  ]
  ,'Виталий': [ 
    '//r.mradx.net/img/72/7CCC26.mp4', 
    '//r.mradx.net/img/F4/28C28F.webm',
    '//r.mradx.net/img/67/26968A.ogg'
  ]
  ,'Виталик': [ 
    '//r.mradx.net/img/C4/D79B7D.mp4', 
    '//r.mradx.net/img/79/329445.webm',
    '//r.mradx.net/img/CF/263B36.ogg'
  ]
  ,'Влад': [ 
    '//r.mradx.net/img/D6/570E37.mp4', 
    '//r.mradx.net/img/80/79B191.webm',
    '//r.mradx.net/img/51/994673.ogg'
  ]
  ,'Владислав': [ 
    '//r.mradx.net/img/23/545307.mp4', 
    '//r.mradx.net/img/D7/10A3CA.webm',
    '//r.mradx.net/img/31/00554E.ogg'
  ]
  ,'Владимир': [ 
    '//r.mradx.net/img/D9/4740EE.mp4', 
    '//r.mradx.net/img/A1/064179.webm',
    '//r.mradx.net/img/BC/94F80B.ogg'
  ]
  ,'Вова': [ 
    '//r.mradx.net/img/87/69D5D2.mp4', 
    '//r.mradx.net/img/64/7D67B6.webm',
    '//r.mradx.net/img/55/24CB40.ogg'
  ]
  ,'Володя': [ 
    '//r.mradx.net/img/27/6FF80A.mp4', 
    '//r.mradx.net/img/DF/E2AB67.webm',
    '//r.mradx.net/img/FA/CED3BD.ogg'
  ]
  ,'Вячеслав': [ 
    '//r.mradx.net/img/69/447F86.mp4', 
    '//r.mradx.net/img/5F/FE2AD3.webm',
    '//r.mradx.net/img/1B/05357A.ogg'
  ]
  ,'Гена': [ 
    '//r.mradx.net/img/BA/0F5833.mp4', 
    '//r.mradx.net/img/EC/6B84B8.webm',
    '//r.mradx.net/img/23/8D74B8.ogg'
  ]
  ,'Генадий': [ 
    '//r.mradx.net/img/13/B3A041.mp4', 
    '//r.mradx.net/img/DD/F54158.webm',
    '//r.mradx.net/img/41/9282E5.ogg'
  ]
  ,'Геннадий': [ 
    '//r.mradx.net/img/A5/0F288E.mp4', 
    '//r.mradx.net/img/66/95A0FA.webm',
    '//r.mradx.net/img/24/425A4C.ogg'
  ]
  ,'Георгий': [ 
    '//r.mradx.net/img/6F/725915.mp4', 
    '//r.mradx.net/img/0B/3D415A.webm',
    '//r.mradx.net/img/02/944F2A.ogg'
  ]
  ,'Глеб': [ 
    '//r.mradx.net/img/D5/B40677.mp4', 
    '//r.mradx.net/img/C2/FFF3FC.webm',
    '//r.mradx.net/img/9D/7281DF.ogg'
  ]
  ,'Григорий': [ 
    '//r.mradx.net/img/80/4B1595.mp4', 
    '//r.mradx.net/img/DC/FF752C.webm',
    '//r.mradx.net/img/CA/9C5F97.ogg'
  ]
  ,'Давид': [ 
    '//r.mradx.net/img/B6/7C614F.mp4', 
    '//r.mradx.net/img/36/8CBE7A.webm',
    '//r.mradx.net/img/9D/20EC6C.ogg'
  ]
  ,'Дамир': [ 
    '//r.mradx.net/img/78/37161D.mp4', 
    '//r.mradx.net/img/35/4E4CE5.webm',
    '//r.mradx.net/img/30/75FFCB.ogg'
  ]
  ,'Даниил': [ 
    '//r.mradx.net/img/DA/485685.mp4', 
    '//r.mradx.net/img/27/569FA1.webm',
    '//r.mradx.net/img/37/C4BB20.ogg'
  ]
  ,'Данил': [ 
    '//r.mradx.net/img/B9/2EFC11.mp4', 
    '//r.mradx.net/img/D2/AE11D8.webm',
    '//r.mradx.net/img/91/F51484.ogg'
  ]
  ,'Данила': [ 
    '//r.mradx.net/img/01/81D6D0.mp4', 
    '//r.mradx.net/img/BB/593A0E.webm',
    '//r.mradx.net/img/3F/D458DD.ogg'
  ]
  ,'Денис': [ 
    '//r.mradx.net/img/A4/4490DF.mp4', 
    '//r.mradx.net/img/CA/A4A9CB.webm',
    '//r.mradx.net/img/62/C2341E.ogg'
  ]
  ,'Дима': [ 
    '//r.mradx.net/img/81/636DEF.mp4', 
    '//r.mradx.net/img/26/C97A5E.webm',
    '//r.mradx.net/img/9D/F2BF61.ogg'
  ]
  ,'Дмитрий': [ 
    '//r.mradx.net/img/28/17970E.mp4', 
    '//r.mradx.net/img/4C/B4F3F5.webm',
    '//r.mradx.net/img/EB/8D2F43.ogg'
  ]
  ,'Дружище': [ 
    '//r.mradx.net/img/EB/4E8788.mp4', 
    '//r.mradx.net/img/F0/6D82D3.webm',
    '//r.mradx.net/img/6A/4A4808.ogg'
  ]
  ,'Евгений': [ 
    '//r.mradx.net/img/D2/D70F46.mp4', 
    '//r.mradx.net/img/A2/2E5184.webm',
    '//r.mradx.net/img/45/555CDD.ogg'
  ]
  ,'Егор': [ 
    '//r.mradx.net/img/2C/A16207.mp4', 
    '//r.mradx.net/img/55/57E0FA.webm',
    '//r.mradx.net/img/4A/267033.ogg'
  ]
  ,'Игорь': [ 
    '//r.mradx.net/img/AC/211C48.mp4', 
    '//r.mradx.net/img/4C/8F4B1A.webm',
    '//r.mradx.net/img/E6/BEEA7F.ogg'
  ]
  ,'Иван': [ 
    '//r.mradx.net/img/9E/FE0D2C.mp4', 
    '//r.mradx.net/img/D4/EA8995.webm',
    '//r.mradx.net/img/DC/0857F8.ogg'
  ]
  ,'Женя': [ 
    '//r.mradx.net/img/F9/32F931.mp4', 
    '//r.mradx.net/img/38/B31BEF.webm',
    '//r.mradx.net/img/F0/16BCF4.ogg'
  ]
  ,'Ильдар': [ 
    '//r.mradx.net/img/6D/6C63BB.mp4', 
    '//r.mradx.net/img/BB/FF3D76.webm',
    '//r.mradx.net/img/82/6B3116.ogg'
  ]
  ,'Илья': [ 
    '//r.mradx.net/img/83/11EF8B.mp4', 
    '//r.mradx.net/img/E9/10EA4D.webm',
    '//r.mradx.net/img/57/9AF0EE.ogg'
  ]
  ,'Кирил': [ 
    '//r.mradx.net/img/3C/5FEE1F.mp4', 
    '//r.mradx.net/img/0A/05C84B.webm',
    '//r.mradx.net/img/92/8BB6D3.ogg'
  ]
  ,'Кирилл': [ 
    '//r.mradx.net/img/CE/7F2071.mp4', 
    '//r.mradx.net/img/22/15875B.webm',
    '//r.mradx.net/img/A4/95E4C0.ogg'
  ]
  ,'Коля': [ 
    '//r.mradx.net/img/66/574473.mp4', 
    '//r.mradx.net/img/69/870775.webm',
    '//r.mradx.net/img/9E/CC131B.ogg'
  ]
  ,'Константин': [ 
    '//r.mradx.net/img/61/3B0A9A.mp4', 
    '//r.mradx.net/img/F8/DF1E79.webm',
    '//r.mradx.net/img/78/6CF0B4.ogg'
  ]
  ,'Костя': [ 
    '//r.mradx.net/img/F4/11BD05.mp4', 
    '//r.mradx.net/img/D0/511A0A.webm',
    '//r.mradx.net/img/5B/627ADC.ogg'
  ]
  ,'Лев': [ 
    '//r.mradx.net/img/6A/27518D.mp4', 
    '//r.mradx.net/img/42/DFE2D6.webm',
    '//r.mradx.net/img/A6/F16DA8.ogg'
  ]
  ,'Леонид': [ 
    '//r.mradx.net/img/4D/72A1F7.mp4', 
    '//r.mradx.net/img/6D/895200.webm',
    '//r.mradx.net/img/4D/226E79.ogg'
  ]
  ,'Леша': [ 
    '//r.mradx.net/img/23/B2A2BF.mp4', 
    '//r.mradx.net/img/C2/DAD50E.webm',
    '//r.mradx.net/img/09/14DD11.ogg'
  ]
  ,'Леха': [ 
    '//r.mradx.net/img/2C/BB5900.mp4',
    '//r.mradx.net/img/D0/6CF36B.webm',
    '//r.mradx.net/img/18/37B561.ogg'
  ]
  ,'Макс': [ 
    '//r.mradx.net/img/B7/1B4353.mp4', 
    '//r.mradx.net/img/3D/1925C2.webm',
    '//r.mradx.net/img/C0/93FFF3.ogg'
  ]
  ,'Марат': [ 
    '//r.mradx.net/img/39/470E9C.mp4', 
    '//r.mradx.net/img/2F/0B7ED6.webm',
    '//r.mradx.net/img/86/F8530F.ogg'
  ]
  ,'Марк': [ 
    '//r.mradx.net/img/2F/EA9F87.mp4', 
    '//r.mradx.net/img/28/120039.webm',
    '//r.mradx.net/img/CA/A75FF7.ogg'
  ]
  ,'Михаил': [ 
    '//r.mradx.net/img/C3/CEFFF9.mp4', 
    '//r.mradx.net/img/41/864621.webm',
    '//r.mradx.net/img/82/43BA5C.ogg'
  ]
  ,'Миша': [ 
    '//r.mradx.net/img/2D/6AB81D.mp4', 
    '//r.mradx.net/img/B2/B73C95.webm',
    '//r.mradx.net/img/73/42E840.ogg'
  ]
  ,'Назар': [ 
    '//r.mradx.net/img/86/3B5D49.mp4', 
    '//r.mradx.net/img/D1/F9092B.webm',
    '//r.mradx.net/img/F8/59C6F8.ogg'
  ]
  ,'Никита': [ 
    '//r.mradx.net/img/99/8C6EEE.mp4', 
    '//r.mradx.net/img/86/DA0ED4.webm',
    '//r.mradx.net/img/27/13C95D.ogg'
  ]
  ,'Никитка': [ 
    '//r.mradx.net/img/62/75C5A6.mp4', 
    '//r.mradx.net/img/A8/3FB309.webm',
    '//r.mradx.net/img/A9/68062C.ogg'
  ]
  ,'Николай': [ 
    '//r.mradx.net/img/C7/108162.mp4', 
    '//r.mradx.net/img/41/C200CA.webm',
    '//r.mradx.net/img/83/080245.ogg'
  ]
  ,'Олег': [ 
    '//r.mradx.net/img/B5/71BB29.mp4', 
    '//r.mradx.net/img/B8/F7B865.webm',
    '//r.mradx.net/img/B1/0D7432.ogg'
  ]
  ,'Олександр': [ 
    '//r.mradx.net/img/1F/FADAB9.mp4', 
    '//r.mradx.net/img/A2/AA63EC.webm',
    '//r.mradx.net/img/83/0E4350.ogg'
  ]
  ,'Павел': [ 
    '//r.mradx.net/img/D0/44C692.mp4', 
    '//r.mradx.net/img/4E/A48C5C.webm',
    '//r.mradx.net/img/4C/A4F0DD.ogg'
  ]
  ,'Паша': [ 
    '//r.mradx.net/img/DD/022418.mp4', 
    '//r.mradx.net/img/E2/64E442.webm',
    '//r.mradx.net/img/36/0D1E59.ogg'
  ]
  ,'Петр': [ 
    '//r.mradx.net/img/94/60DB92.mp4', 
    '//r.mradx.net/img/4A/FCFBC4.webm',
    '//r.mradx.net/img/F0/4845A0.ogg'
  ]
  ,'Пётр': [ 
    '//r.mradx.net/img/11/E26B78.mp4', 
    '//r.mradx.net/img/D1/2FD98A.webm',
    '//r.mradx.net/img/33/CF3E50.ogg'
  ]
  ,'Петя': [ 
    '//r.mradx.net/img/ED/C23427.mp4', 
    '//r.mradx.net/img/A5/EFE23C.webm',
    '//r.mradx.net/img/ED/BAF231.ogg'
  ]
  ,'Рамиль': [ 
    '//r.mradx.net/img/DA/2D2F12.mp4', 
    '//r.mradx.net/img/60/E66E16.webm',
    '//r.mradx.net/img/85/31DF1C.ogg'
  ]
  ,'Ренат': [ 
    '//r.mradx.net/img/ED/BAF231.ogg', 
    '//r.mradx.net/img/AF/2D6FDA.webm',
    '//r.mradx.net/img/59/6D321A.ogg'
  ]
  ,'Ринат': [ 
    '//r.mradx.net/img/0F/B3ED5D.mp4', 
    '//r.mradx.net/img/7E/92A792.webm',
    '//r.mradx.net/img/E7/EAF379.ogg'
  ]
  ,'Рома': [ 
    '//r.mradx.net/img/37/E10B36.mp4', 
    '//r.mradx.net/img/8C/77055B.webm',
    '//r.mradx.net/img/B5/EFF812.ogg'
  ]
  ,'Роман': [ 
    '//r.mradx.net/img/26/237B04.mp4', 
    '//r.mradx.net/img/68/7C5CB1.webm',
    '//r.mradx.net/img/7D/19848E.ogg'
  ]
  ,'Руслан': [ 
    '//r.mradx.net/img/F5/E8EE9F.mp4', 
    '//r.mradx.net/img/62/06C357.webm',
    '//r.mradx.net/img/13/123C85.ogg'
  ]
  ,'Рустам': [ 
    '//r.mradx.net/img/B9/30B01E.mp4', 
    '//r.mradx.net/img/65/73AED2.webm',
    '//r.mradx.net/img/CE/CFFAB1.ogg'
  ]
  ,'Саня': [ 
    '//r.mradx.net/img/66/AA2830.mp4', 
    '//r.mradx.net/img/20/C44961.webm',
    '//r.mradx.net/img/E4/54FDCA.ogg'
  ]
  ,'Саша': [ 
    '//r.mradx.net/img/CE/169C1E.mp4', 
    '//r.mradx.net/img/F2/8C0095.webm',
    '//r.mradx.net/img/25/BD0643.ogg'
  ]
  ,'Семен': [ 
    '//r.mradx.net/img/25/BD0643.ogg', 
    '//r.mradx.net/img/66/7F59E8.webm',
    '//r.mradx.net/img/BB/538FF5.ogg'
  ]
  ,'Сергей': [ 
    '//r.mradx.net/img/BB/BB0D10.mp4', 
    '//r.mradx.net/img/6A/FA2C78.webm',
    '//r.mradx.net/img/B9/BE118A.ogg'
  ]
  ,'Серега': [ 
    '//r.mradx.net/img/60/FD17DA.mp4', 
    '//r.mradx.net/img/FB/D9CC0A.webm',
    '//r.mradx.net/img/E3/85FBB7.ogg'
  ]
  ,'Сережа': [ 
    '//r.mradx.net/img/16/088FE3.mp4', 
    '//r.mradx.net/img/6F/D23621.webm',
    '//r.mradx.net/img/48/5BF57F.ogg'
  ]
  ,'Слава': [ 
    '//r.mradx.net/img/47/69F303.mp4', 
    '//r.mradx.net/img/51/59268E.webm',
    '//r.mradx.net/img/F8/434A6B.ogg'
  ]
  ,'Славик': [ 
    '//r.mradx.net/img/DD/B63F5A.mp4', 
    '//r.mradx.net/img/9F/382516.webm',
    '//r.mradx.net/img/8C/9E76CF.ogg'
  ]
  ,'Станислав': [ 
    '//r.mradx.net/img/8C/E936A9.mp4', 
    '//r.mradx.net/img/81/FECB98.webm',
    '//r.mradx.net/img/50/D6920D.ogg'
  ]
  ,'Стас': [ 
    '//r.mradx.net/img/24/16A5F0.mp4', 
    '//r.mradx.net/img/36/779981.webm',
    '//r.mradx.net/img/B2/6D57A1.ogg'
  ]
  ,'Степан': [ 
    '//r.mradx.net/img/E6/B397AE.mp4', 
    '//r.mradx.net/img/66/30F66E.webm',
    '//r.mradx.net/img/9B/0B8FEE.ogg'
  ]
  ,'Тарас': [ 
    '//r.mradx.net/img/D3/ACF083.mp4', 
    '//r.mradx.net/img/79/BFE72C.webm',
    '//r.mradx.net/img/51/2E1B8E.ogg'
  ]
  ,'Тема': [ 
    '//r.mradx.net/img/33/FC7086.mp4', 
    '//r.mradx.net/img/38/685E24.webm',
    '//r.mradx.net/img/C4/0FF205.ogg'
  ]
  ,'Тимофей': [ 
    '//r.mradx.net/img/D9/8CAA87.mp4', 
    '//r.mradx.net/img/AB/BD5579.webm',
    '//r.mradx.net/img/79/DA21D1.ogg'
  ]
  ,'Тимур': [ 
    '//r.mradx.net/img/49/DFCF1A.mp4', 
    '//r.mradx.net/img/53/249B42.webm',
    '//r.mradx.net/img/2E/3E5F0E.ogg'
  ]
  ,'Федор': [ 
    '//r.mradx.net/img/66/05933D.mp4', 
    '//r.mradx.net/img/BB/D35457.webm',
    '//r.mradx.net/img/83/0C8D51.ogg'
  ]
  ,'Фёдор': [ 
    '//r.mradx.net/img/D1/144A37.mp4', 
    '//r.mradx.net/img/BC/211659.webm',
    '//r.mradx.net/img/9F/18A8C2.ogg'
  ]
  ,'Эдуард': [ 
    '//r.mradx.net/img/23/4A7416.mp4', 
    '//r.mradx.net/img/FC/F51E07.webm',
    '//r.mradx.net/img/FE/FA77C5.ogg'
  ]
  ,'Эльдар': [ 
    '//r.mradx.net/img/45/C66E81.mp4', 
    '//r.mradx.net/img/23/AD6BD9.webm',
    '//r.mradx.net/img/FA/5866B5.ogg'
  ]
  ,'Юра': [ 
    '//r.mradx.net/img/74/BDBFB8.mp4', 
    '//r.mradx.net/img/D4/3576B8.webm',
    '//r.mradx.net/img/AD/E69730.ogg'
  ]
  ,'Юрий': [ 
    '//r.mradx.net/img/18/9AD67C.mp4', 
    '//r.mradx.net/img/5A/DDD049.webm',
    '//r.mradx.net/img/03/DDF39B.ogg'
  ]
  ,'Ян': [ 
    '//r.mradx.net/img/D6/26FDD1.mp4', 
    '//r.mradx.net/img/F5/F2C904.webm',
    '//r.mradx.net/img/A2/18AC81.ogg'
  ]
  ,'Ярослав': [ 
    '//r.mradx.net/img/B5/3EEA18.mp4', 
    '//r.mradx.net/img/7F/1E1640.webm',
    '//r.mradx.net/img/69/A2EEAF.ogg'
  ]
  , 'Александра': [
    '//r.mradx.net/img/0B/7198FC.mp4',
    '//r.mradx.net/img/ED/FF9A8F.webm',
    '//r.mradx.net/img/56/D10B4E.ogg'
  ]
  , 'Алеся': [
    '//r.mradx.net/img/75/7058FF.mp4',
    '//r.mradx.net/img/AB/05A2A8.webm',
    '//r.mradx.net/img/95/7EA996.ogg'
    ]
  , 'Анастасия': [
    '//r.mradx.net/img/9E/D0431C.mp4',
    '//r.mradx.net/img/C0/6D584A.webm',
    '//r.mradx.net/img/FE/757C75.ogg'
    ]
  , 'Анна': [
    '//r.mradx.net/img/73/A5E1B0.mp4',
    '//r.mradx.net/img/14/2F51FC.webm',
    '//r.mradx.net/img/28/CF00FB.ogg'
    ]
  , 'Антонина': [
    '//r.mradx.net/img/9A/147D64.mp4',
    '//r.mradx.net/img/91/5DEF09.webm',
    '//r.mradx.net/img/E5/5344DD.ogg'
    ]
  , 'Валентина': [
    '//r.mradx.net/img/FE/18BF2E.mp4',
    '//r.mradx.net/img/C9/568A14.webm',
    '//r.mradx.net/img/9D/F1EB6F.ogg'
    ]
  , 'Валерия': [
    '//r.mradx.net/img/04/505D14.mp4',
    '//r.mradx.net/img/2D/96973D.webm',
    '//r.mradx.net/img/2C/E908DA.ogg'
    ]
  , 'Виктория': [
    '//r.mradx.net/img/2F/E31BEE.mp4',
    '//r.mradx.net/img/06/18FD29.webm',
    '//r.mradx.net/img/CB/DB2B11.ogg'
    ]
  , 'Галина': [
    '//r.mradx.net/img/D6/7EA749.mp4',
    '//r.mradx.net/img/AC/535231.webm',
    '//r.mradx.net/img/87/834845.ogg'
    ]
  , 'Дарья': [
    '//r.mradx.net/img/8E/2F6CEF.mp4',
    '//r.mradx.net/img/59/0CC520.webm',
    '//r.mradx.net/img/A1/A2F59E.ogg'
    ]
  , 'Евгения': [
    '//r.mradx.net/img/78/DA38A0.mp4',
    '//r.mradx.net/img/4C/BE46E0.webm',
    '//r.mradx.net/img/0E/BCBB29.ogg'
    ]
  , 'Екатерина': [
    '//r.mradx.net/img/A1/CDD004.mp4',
    '//r.mradx.net/img/B9/359EEE.webm',
    '//r.mradx.net/img/F7/3C46FE.ogg'
    ]
  , 'Елена': [
    '//r.mradx.net/img/31/6CC21D.mp4',
    '//r.mradx.net/img/10/623F20.webm',
    '//r.mradx.net/img/C6/8FC39A.ogg'
    ]
  , 'Елизавета': [
    '//r.mradx.net/img/20/593291.mp4',
    '//r.mradx.net/img/A0/45427E.webm',
    '//r.mradx.net/img/B2/F4FC34.ogg'
    ]
  , 'Ирина': [
    '//r.mradx.net/img/18/F87F90.mp4',
    '//r.mradx.net/img/35/07B79C.webm',
    '//r.mradx.net/img/FD/8F08EB.ogg'
    ]
  , 'Инна': [
    '//r.mradx.net/img/92/5CCB5C.mp4',
    '//r.mradx.net/img/D9/99DB87.webm',
    '//r.mradx.net/img/1C/CC1027.ogg'
    ]
  , 'Катерина': [
    '//r.mradx.net/img/3B/86A921.mp4',
    '//r.mradx.net/img/C0/F73D3B.webm',
    '//r.mradx.net/img/14/5EF738.ogg'
    ]
  , 'Кристина': [
    '//r.mradx.net/img/0E/F5BA0D.mp4',
    '//r.mradx.net/img/3A/47769B.webm',
    '//r.mradx.net/img/6A/2F826C.ogg'
    ]
  , 'Ксения': [
    '//r.mradx.net/img/93/689796.mp4',
    '//r.mradx.net/img/C8/B2A358.webm',
    '//r.mradx.net/img/37/6696F9.ogg'
    ]
  , 'Лидия': [
    '//r.mradx.net/img/B8/406CF1.mp4',
    '//r.mradx.net/img/1B/4E88C0.webm',
    '//r.mradx.net/img/F0/80AE1E.ogg'
    ]
  , 'Людмила': [
    '//r.mradx.net/img/18/009239.mp4',
    '//r.mradx.net/img/C8/B71DAE.webm',
    '//r.mradx.net/img/E5/ECB58F.ogg'
    ]
  , 'Мария': [
    '//r.mradx.net/img/69/C4AB3B.mp4',
    '//r.mradx.net/img/71/C6BE20.webm',
    '//r.mradx.net/img/9D/29C280.ogg'
    ]
  , 'Маргарита': [
    '//r.mradx.net/img/B3/82B0C4.mp4',
    '//r.mradx.net/img/78/8D1B7C.webm',
    '//r.mradx.net/img/BE/4B7DCD.ogg'
    ]
  , 'Надежда': [
    '//r.mradx.net/img/41/D57AEC.mp4',
    '//r.mradx.net/img/98/03F478.webm',
    '//r.mradx.net/img/2B/AB8C08.ogg'
    ]
  , 'Наталья': [
    '//r.mradx.net/img/70/B356A4.mp4',
    '//r.mradx.net/img/A7/E4286F.webm',
    '//r.mradx.net/img/27/986EDF.ogg'
    ]
  , 'Оксана': [
    '//r.mradx.net/img/DD/58F937.mp4',
    '//r.mradx.net/img/EC/A74216.webm',
    '//r.mradx.net/img/70/CD8A15.ogg'
    ]
  , 'Олеся': [
    '//r.mradx.net/img/40/C43A83.mp4',
    '//r.mradx.net/img/D0/410ACB.webm',
    '//r.mradx.net/img/F3/17B25C.ogg'
    ]
  , 'Ольга': [
    '//r.mradx.net/img/5F/530165.mp4',
    '//r.mradx.net/img/5B/00C5B4.webm',
    '//r.mradx.net/img/22/59FDC0.ogg'
    ]
  , 'Светлана': [
    '//r.mradx.net/img/ED/928C13.mp4',
    '//r.mradx.net/img/71/4BAA80.webm',
    '//r.mradx.net/img/E0/FD36B2.ogg'
    ]
  , 'София': [
    '//r.mradx.net/img/13/0C992A.mp4',
    '//r.mradx.net/img/A0/CE6FC5.webm',
    '//r.mradx.net/img/8B/2E706B.ogg'
    ]
  , 'Софья': [
    '//r.mradx.net/img/04/200EEF.mp4',
    '//r.mradx.net/img/EA/F3C75F.webm',
    '//r.mradx.net/img/54/4AE5B8.ogg'
    ]
  , 'Татьяна': [
    '//r.mradx.net/img/F0/328C79.mp4',
    '//r.mradx.net/img/B6/C4FEBB.webm',
    '//r.mradx.net/img/39/98EDC2.ogg'
    ]
  , 'Юлия': [
    '//r.mradx.net/img/D6/883D6A.mp4',
    '//r.mradx.net/img/99/28884B.webm',
    '//r.mradx.net/img/C0/5608B8.ogg'
    ]
  , 'Лёха': [
    '//r.mradx.net/img/3E/0854C4.mp4', 
    '//r.mradx.net/img/31/115762.webm',
    '//r.mradx.net/img/12/FF42E1.ogg'
    ]
  , 'Никитос': [
    '//r.mradx.net/img/62/75C5A6.mp4',
    '//r.mradx.net/img/A8/3FB309.webm',
    '//r.mradx.net/img/2F/EB8208.ogg'
  ]
  , 'Дорогая': [
    '//r.mradx.net/img/56/A11B8B.mp4',
    '//r.mradx.net/img/35/9088EF.webm',
    '//r.mradx.net/img/23/6A223B.ogg'
  ]
  , 'Максим': [
    '//r.mradx.net/img/48/93A7EE.mp4',
    '//r.mradx.net/img/28/CEDF0B.webm',
    '//r.mradx.net/img/B5/F31218.ogg'
  ]
  , 'Вика': [
    '//r.mradx.net/img/9F/80B858.mp4',
    '//r.mradx.net/img/01/474578.webm',
    '//r.mradx.net/img/80/C3F83D.ogg'
  ]
}
