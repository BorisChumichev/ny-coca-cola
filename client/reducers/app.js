import R from 'ramda'
import { merge } from 'ramda'
import qs from 'query-string'
import { trackEvent, trackPageview } from '../util.js'

const recipient = qs.parse(location.search).recipient || null

const defaultState = {
  route: recipient
    ? 'congratulation'
    : 'entry',
  isLoading: false,
  recipientName: recipient,
  nameSelectorIsOpened: false
}

trackPageview(defaultState.route)

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'TOGGLE_NAME_SELECTOR':
      return merge(state, { nameSelectorIsOpened: !state.nameSelectorIsOpened })

    case 'TOGGLE_LOADING':
      return merge(state, { isLoading: !state.isLoading })

    case 'SET_ROUTE':
      trackPageview(action.route)
      return merge(state, { route: action.route })

    case 'SET_NAME':
      trackEvent('Name Selected', action.name)
      return merge(state, { recipientName: action.name, nameSelectorIsOpened: false })

    default:
      return state
  }
}

export default reducer
