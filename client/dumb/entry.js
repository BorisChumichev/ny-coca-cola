import './entry.sass'

import BackgroundWrapper from 'dumb/background-wrapper'
import CocaColaLogo from 'dumb/coca-cola-logo'
import Button from 'dumb/button'

import React, { Component } from 'react';
import { trackEvent } from '../util.js'

const nbsp = '\u00A0'

class Entry extends Component {
  render() {
    const { onClickSelectName, onClickCreate, recipientName } = this.props
    return (
      <BackgroundWrapper>
        <div className='entry animated fadeIn'>
          <CocaColaLogo />
          <div className='entry-title'>
            Новогоднее<br/>поздравление<br/>от{nbsp}Coca-Cola<br/>и{nbsp}Димы{nbsp}Билана
          </div>
          <div className='entry-subtitle'>
            Поздравь{nbsp}друзей{nbsp}и{nbsp}близких<br/>с{nbsp}наступающим{nbsp}Новым{nbsp}годом!
          </div>
          <div className='entry-buttonsNest'>
            <Button onPress={() => {
                trackEvent('Clicked Select Name', 'clicked select name')
                onClickSelectName()
              }
            } modifier='arrow'>{recipientName ? recipientName : 'Выбери имя'}</Button>
            <Button onPress={() => {
              onClickCreate()
              trackEvent('Clicked Create on Entry Page', 'clicked create')
            }} modifier='blink'>Создать поздравление</Button>
          </div>
        </div>
      </BackgroundWrapper>
    )
  }
}

export default Entry;
