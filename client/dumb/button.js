import './button.sass'

import React, { Component } from 'react';

const blink1Image = require('../../public/blink1.png')
  , blink2Image = require('../../public/blink2.png')

class Button extends Component {

  componentDidMount() {
    let i = 0
    if (this.props.modifier === 'blink')
      setInterval(() => {
        if (this && this.refs.bg)
          this.refs.bg.style.backgroundImage = `url(${(i++ % 2) ? blink1Image : blink2Image})`
      }, 180)
  }

  render() {
    const { modifier, onPress } = this.props

    return (
      <div ref='bg' onClick={onPress} className={`button button_${modifier}`}>
        {this.props.children}
      </div>
    )
  }
}

export default Button;
