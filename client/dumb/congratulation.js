import './congratulation.sass'

import React, { Component } from 'react';

import BackgroundWrapper from 'dumb/background-wrapper'
import CocaColaLogo from 'dumb/coca-cola-logo'
import $ from 'jquery'
import { debounce, partial } from 'lodash'
import { resolveVideo, isMobile, trackEvent } from '../util.js'
import Button from 'dumb/button'


const nbsp = '\u00A0'

class Congratulation extends Component {
  componentDidMount() {
    trackEvent('Video Received by Recipient Name', this.props.recipientName)

    const heightToWidthRatio = 0.5625
      , $video = $(this.refs.video)
      , $videoWrapper = $(this.refs.videoWrapper)
      , $window = $(window)

      , adjustVideoSizeAndPosition = () => {
          $videoWrapper.height() / $videoWrapper.width() > heightToWidthRatio
            ? ( $video.attr('height', 'auto')
              , $video.attr('width', $videoWrapper.width())
              , $video.css('margin-top', ($videoWrapper.height() - $video.height()) * .5)
              )
            : ( $video.attr('height', $videoWrapper.height())
              , $video.attr('width', 'auto')
              , $video.css('margin-top', '0px')
              )
        }

    if (!isMobile()) {
      $window.resize(debounce(adjustVideoSizeAndPosition, 50))
      adjustVideoSizeAndPosition()
    }

    $video.on("pause", partial(trackEvent, "Received Video", "pause"))
    $video.on("play", partial(trackEvent, "Received Video", "play"))
    $video.on("ended", partial(trackEvent, "Received Video", "ended"))

    let milestones = 0
    $video.on("timeupdate", evt => {
      if ((evt.target.currentTime / evt.target.duration) > .25 && milestones < 1)
        { trackEvent("Received Video", `milestone 25%`); milestones++ }
      if ((evt.target.currentTime / evt.target.duration) > .50 && milestones < 2)
        { trackEvent("Received Video", `milestone 50%`); milestones++ }
      if ((evt.target.currentTime / evt.target.duration) > .75 && milestones < 3)
        { trackEvent("Received Video", `milestone 75%`); milestones++ }
    })

    let videoClicked = false
    $video.click(() => {
        if (videoClicked) return;
        $video[0].play()
        $video.attr('controls', 'true')
      }
    )
  }

  render() {
    const { onClickCreate, recipientName } = this.props
    return (
      <BackgroundWrapper modifier='blur'>
        <div className='shareScreen'>
          <div className='congratulation-logo'><CocaColaLogo /></div>
          { isMobile()
            ? <div className='congratulation-title'>Дима Билан поздравляет с{nbsp}Новым годом!</div>
            : <div className='congratulation-title'>Дима{nbsp}Билан{nbsp}поздравляет<br/>с{nbsp}Новым{nbsp}годом!</div>
          }
          <div ref='videoWrapper' className='congratulation-videoNest'>
            <video autoPlay={true} poster={require('public/poster.jpg')} ref='video' className='shareScreen-video'>
              <source src={resolveVideo(recipientName, 'webm')} type="video/webm" />
              <source src={resolveVideo(recipientName, 'ogg')} type="video/ogg" />
              <source src={resolveVideo(recipientName, 'mp4')} type="video/mp4" />
            </video>
          </div>
          <div className='congratulation-footer'>
            <Button onPress={() => {
                trackEvent('Recipient Clicked Create by Recipient Name', recipientName)
                onClickCreate()
              }
            } modifier='blink'>Создать поздравление</Button>
          </div>
        </div>
      </BackgroundWrapper>
    )
  }
}

export default Congratulation;

