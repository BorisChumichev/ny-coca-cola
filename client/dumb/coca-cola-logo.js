import './coca-cola-logo.sass'

import React, { Component } from 'react';

class CocaColaLogo extends Component {
  render() {
    return (
      <div style={{
        cursor: location.hostname === 'coca-cola.ny.mail.ru' ? 'pointer' : 'default'
      }} onClick={() => {
        if (location.hostname === 'coca-cola.ny.mail.ru')
          window.open(`https://cocacola.solution.weborama.fr/fcgi-bin/dispatch.fcgi?a.A=cl&a.si=91&a.te=2474&a.ra=${Math.random()}&g.lu=`)
      }} className='cocaColaLogo'></div>
    )
  }
}

export default CocaColaLogo;
