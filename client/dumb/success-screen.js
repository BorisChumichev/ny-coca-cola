import './success-screen.sass'

import BackgroundWrapper from 'dumb/background-wrapper'
import CocaColaLogo from 'dumb/coca-cola-logo'
import Button from 'dumb/button'

import React, { Component } from 'react'

const nbsp = '\u00A0'

class SuccessScreen extends Component {
  render() {
    const { onRepeat } = this.props
    return (
      <BackgroundWrapper>
        <div className='successScreen'>
          <CocaColaLogo />
          <div className='successScreen-title'>
            Ваше<br/>поздравление<br/>отправлено
          </div>
          <div className='successScreen-buttonsNest'>
            <Button onPress={onRepeat} modifier='blink'>Создать еще поздравление</Button>
          </div>
        </div>
      </BackgroundWrapper>
    )
  }
}

export default SuccessScreen;
