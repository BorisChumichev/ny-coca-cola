import './name-selector.sass'

import React, { Component } from 'react';
import { map, prop, filter, take, uniq, reject, partial } from 'ramda'
import $ from 'jquery'
window.$ = $

import { isMobile } from '../util.js'
import Button from 'dumb/button'

const isM = isMobile()

$.fn.scrollTo = function(elem, speed) { 
    $(this).animate({
        scrollTop:  $(this).scrollTop() - $(this).offset().top + $(elem).offset().top 
    }, speed == undefined ? 1000 : speed); 
    return this; 
};

import videoset from 'constants/videoset';

const cyrillicRegularExpression = /[А-я]/
  , startsWithCyrillic = str => cyrillicRegularExpression.test(str[0])
  , names = reject(str => /[\(]|Дружище|Дорогая/.test(str), filter(startsWithCyrillic, map(prop('name'), videoset)))
  , validSortedCharacters = uniq(map(take(1), names)).sort((a, b) =>
      (a < b)
        ? -1
        : (a > b)
          ? 1
          : 0
    )

class NameSelector extends Component {
  constructor() {
    super()
    this.state = {
      mobileFooterOpened: false
    }
  }
  
  componentWillReceiveProps(nextProps) {
    if (isM && nextProps.isOpened) {
      setTimeout(function () {
        const $sections = $('.nameSelector-bodySection')
        $('.nameSelector-body').scroll(evt => {
          $sections.each(function () {
            if (this.getBoundingClientRect().top < 0)
              this.firstChild.style.position = 'fixed'
            else
              this.firstChild.style.position = 'absolute'
          })
        })
      }, 1)
    }
  }

  render() {
    const { isOpened, onNameSelect, toggleNameSelector } = this.props
    return isOpened
      ? <div className='nameSelector-overlay animated fadeIn'>
          <div className='nameSelector animated fadeInUp'>
            <div className='nameSelector-heading'>
              <div className='nameSelector-cross' onClick={toggleNameSelector}></div>
              <div className='nameSelector-title'>Выбери имя</div>
              <div className='nameSelector-characters'>{map(
                  character =>
                    <div
                      key={character}
                      onClick={() => $('.nameSelector-body').scrollTo(`#section${character}`) }
                      className='nameSelector-character'>
                        {character}
                    </div>,
                  validSortedCharacters
              )}</div>
            </div>
            
            <div className='nameSelector-body'>{map(
              character =>
                <div key={character} id={`section${character}`} className='nameSelector-bodySection'>
                  <div className='nameSelector-sectionTitle'>{character}</div>
                  <div className='nameSelector-sectionColumns'>{map(
                    name => <div key={Math.random()} onClick={partial(onNameSelect, [name])} className='nameSelector-name'>{name}</div>,
                    filter(str => str[0] === character, names)
                  )}</div>
                </div>,
              validSortedCharacters
            )}</div>
            
            { isM
              ? this.state.mobileFooterOpened
                ? <div className='nameSelector-footerMobile animated fadeInUp'>
                    <div>Выбери обращение</div>
                    <Button onPress={partial(onNameSelect, ['Дружище'])} modifier='arrow'>Дружище</Button>
                    <Button onPress={partial(onNameSelect, ['Дорогая'])} modifier='arrow'>Дорогая</Button>
                    <div onClick={() => this.setState({ mobileFooterOpened: false })} className='nameSelector-footerMobileMinus'></div>
                  </div>
                : <div className='nameSelector-footerMobile animated fadeInUp'>
                    <div>Не нашли нужное имя?</div>
                    <div onClick={() => this.setState({ mobileFooterOpened: true })} className='nameSelector-footerMobilePlus'></div>
                  </div>
              : <div className='nameSelector-footer'>
                  Не нашли нужное имя?<br />
                  Выбери следующее обращение: <div onClick={partial(onNameSelect, ['Дружище'])} className="nameSelector-footerLink">Дружище</div> или <div onClick={partial(onNameSelect, ['Дорогая'])} className="nameSelector-footerLink">Дорогая</div>
                </div>
            }

          </div>
        </div>
      : null
  }
}

export default NameSelector;
