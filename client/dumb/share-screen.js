import './share-screen.sass'

import React, { Component } from 'react';
import Clipboard from 'clipboard'

import BackgroundWrapper from 'dumb/background-wrapper'
import CocaColaLogo from 'dumb/coca-cola-logo'
import $ from 'jquery'
import { debounce, partial } from 'lodash'
import { share, resolveVideo, isMobile, trackEvent, isMacLike } from '../util.js'


const nbsp = '\u00A0'

class ShareScreen extends Component {
  constructor() {
    super()
    this.state = { clipboardPopup: false, 'cbError': false }
  }

  componentWillUnmount () {
    if (window.MRGCanvas) MRGCanvas.removeVideo(this.refs.video)
  }

  componentDidMount() {
    trackEvent('Created Video for Name', this.props.recipientName)

    const heightToWidthRatio = 0.5625
      , $video = $(this.refs.video)
      , $videoWrapper = $(this.refs.videoWrapper)
      , $window = $(window)

      , adjustVideoSizeAndPosition = () => {
          $videoWrapper.height() / $videoWrapper.width() > heightToWidthRatio
            ? ( $video.attr('height', 'auto')
              , $video.attr('width', $videoWrapper.width())
              , $video.css('margin-top', ($videoWrapper.height() - $video.height()) * .5)
              )
            : ( $video.attr('height', $videoWrapper.height())
              , $video.attr('width', 'auto')
              , $video.css('margin-top', '0px')
              )
        }

    if (!isMobile()) {
      $window.resize(debounce(adjustVideoSizeAndPosition, 50))
      adjustVideoSizeAndPosition()
    }

    if (window.MRGCanvas) MRGCanvas.addVideo(this.refs.video)

    $video.on("pause", partial(trackEvent, "Video Preview", "pause"))
    $video.on("play", partial(trackEvent, "Video Preview", "play"))
    $video.on("ended", partial(trackEvent, "Video Preview", "ended"))

    let milestones = 0
    $video.on("timeupdate", evt => {
      if ((evt.target.currentTime / evt.target.duration) > .25 && milestones < 1)
        { trackEvent("Video Preview", `milestone 25%`); milestones++ }
      if ((evt.target.currentTime / evt.target.duration) > .50 && milestones < 2)
        { trackEvent("Video Preview", `milestone 50%`); milestones++ }
      if ((evt.target.currentTime / evt.target.duration) > .75 && milestones < 3)
        { trackEvent("Video Preview", `milestone 75%`); milestones++ }
    })

    const cb = new Clipboard('.shareScreen-sharerIcon_clipboard')

    cb.on('error', (e) => {
      this.setState({cbError: true})
    })

    let videoClicked = false
    $video.click(() => {
        if (videoClicked) return;
        $video[0].play()
        $video.attr('controls', 'true')
      }
    )
  }

  showClipboardPopup() {
    this.setState({ clipboardPopup: true })
  }

  hideClipboardPopup() {
    this.setState({ clipboardPopup: false })
  }

  render() {
    const { onShareCompletion, recipientName, goback, sendEmail } = this.props
    return (
      <BackgroundWrapper modifier='blur'>
        <div className='shareScreen animated fadeIn'>
          <div className='shareScreen-logo'><CocaColaLogo /></div>
          <div className='shareScreen-title'>{recipientName}{nbsp}получит<br/>поздравление</div>
          <div className='shareScreen-backButton' onClick={() => {
              trackEvent('Click recreate', 'click recreate')
              goback()
            }
          }>Создать{nbsp}еще<br/>поздравление</div>
          <div ref='videoWrapper' className='shareScreen-videoNest'>
            <video autoPlay={true} poster={require('public/poster.jpg')} ref='video' className='shareScreen-video'>
              <source src={resolveVideo(recipientName, 'webm')} type="video/webm" />
              <source src={resolveVideo(recipientName, 'ogg')} type="video/ogg" />
              <source src={resolveVideo(recipientName, 'mp4')} type="video/mp4" />
            </video>
          </div>
          <div className='shareScreen-footer'>
            { location.search.match(/email/) &&
              <div className='shareScreen-emailField'>
                <input ref='email' placeholder='Введи email' type='email' />
                <div onClick={() => {
                      if (!this.refs.email.value || !this.refs.email.value.length || !/@/.test(this.refs.email.value))
                        return;
                      sendEmail(
                        { name: recipientName
                        , email: this.refs.email.value
                        }
                      )
                    }
                } className='shareScreen-emailButton'>
                </div>
              </div>
            }
            <div className='shareScreen-sharer'>
              {[ 'fb', 'vk', 'ok', 'clipboard' ].map(type =>
                  <div
                    key={type}
                    data-clipboard-text={`https://coca-cola.ny.mail.ru?recipient=${encodeURIComponent(recipientName)}`}
                    onClick={() => {
                      trackEvent('Video Shared by Method', type)
                      trackEvent('Video Shared by Recipient Name', recipientName)
                      share(type, recipientName)
                      type === 'clipboard'
                        ? this.showClipboardPopup()
                        : onShareCompletion()
                    }}
                    className={`shareScreen-sharerIcon shareScreen-sharerIcon_${type}`}>
                  </div>
              )}
            </div>
          </div>
        </div>
        { this.state.clipboardPopup &&
          <div className='shareScreen-clipboardPopup'>
            <div className='shareScreen-clipboardPane'>
              { this.state.cbError
                  ? <span>
                      Нажмите {isMacLike
                        ? <pre style={{ display: 'inline-block', lineHeight: '0px'}}>cmd+c</pre>
                        : <pre style={{ display: 'inline-block', lineHeight: '0px'}}>ctrl+c</pre>
                      },<br/>чтобы скопировать{nbsp}ссылку
                    </span>
                  : <span>
                      Ссылка на поздравление скопирована в{nbsp}буфер{nbsp}обмена
                    </span>
              }
              <br/>
              <div onClick={() => this.hideClipboardPopup()} className='shareScreen-clipboardButton'>Хорошо</div>
            </div>
          </div>
        }
      </BackgroundWrapper>
    )
  }
}

export default ShareScreen;

