import './background-wrapper.sass'

import React, { Component } from 'react';

class BackgroundWrapper extends Component {
  render() {
    const { modifier } = this.props
    return (
      <div className={`backgroundWrapper${modifier ? ' backgroundWrapper_' + modifier : ''}`}>
        {this.props.children}
      </div>
    )
  }
}

export default BackgroundWrapper;
