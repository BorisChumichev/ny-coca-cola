'use strict';

module.exports = app => (req, res, next) => {
  res.render('index', { app })
}
