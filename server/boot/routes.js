'use strict';

module.exports = app =>
  app.use(
    app.loopback.Router()
      .get
        ( '/'
        , require('../middleware/home')(app)
        )
  )
